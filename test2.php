<?php
include_once('connection.php');
$url = $_SERVER['REQUEST_URI'];
if(strrchr($url, '/'))
{
    $str = substr(strrchr($url, '/'), 1);
}
else
{
    $str = strrchr($url, '/');
}
// echo $str;
$qu = mysqli_query($con,"select * from spa_templates where sitename='$str'");
while($row = mysqli_fetch_assoc($qu))
{

 $id = $row["id"];
    $sitename = $row["sitename"];
    $logo = $row["logo"];
    $logo_text=$row["logo_text"];
    $url = $row["url"];
    $title = $row["title"];
    $heading = $row["heading"];
    $caption = $row["caption"];
    $about = $row["about"];
    $services = $row["services"];
    $service_icon1 = $row["service_icon1"];
    $service_text1 = $row["service_text1"];
    $service_description1 = $row["service_description1"];
    $service_icon2 = $row["service_icon2"];
    $service_text2 = $row["service_text2"];
    $service_description2 = $row["service_description2"];
    $service_icon3 = $row["service_icon3"];
    $service_text3 = $row["service_text3"];
    $service_description3 = $row["service_description3"];
    $features = $row["features"];

    $feature_text1 = $row["feature_text1"];
    $feature_icon1 = $row["feature_icon1"];
    $feature_description1 = $row["feature_description1"];
   
    $feature_text2 = $row["feature_text2"];
    $feature_icon2 = $row["feature_icon2"];
    $feature_description2 = $row["feature_description2"];

    $feature_text3 = $row["feature_text3"];
    $feature_icon3 = $row["feature_icon3"];
    $feature_description3 = $row["feature_description3"];

    $feature_text4 = $row["feature_text4"];
    $feature_icon4 = $row["feature_icon4"];
    $feature_description4 = $row["feature_description4"];

    $feature_text5 = $row["feature_text5"];
    $feature_icon5 = $row["feature_icon5"];
    $feature_description5 = $row["feature_description5"];

    $feature_text6 = $row["feature_text6"];
    $feature_icon6 = $row["feature_icon6"];
    $feature_description6 = $row["feature_description6"];

    // $testimonial_icon1 = $row["testimonial_icon1"];
    // $testimonial1 = $row["testimonial1"];
    // $testimonial_name1 = $row["testimonial_name1"];

    // $testimonial_icon2 = $row["testimonial_icon2"];
    // $testimonial2 = $row["testimonial2"];
    // $testimonial_name2 = $row["testimonial_name2"];

    // $testimonial_icon3 = $row["testimonial_icon3"];
    // $testimonial3 = $row["testimonial3"];
    // $testimonial_name3 = $row["testimonial_name3"];

    // $testimonial_icon4 = $row["testimonial_icon4"];
    // $testimonial4 = $row["testimonial4"];
    // $testimonial_name4 = $row["testimonial_name4"];


    $team_image1 = $row["team_image1"];
    $team_name1 = $row["team_name1"];
    $team_role1 = $row["team_role1"];

    $team_image2 = $row["team_image2"];
    $team_name2 = $row["team_name2"];
    $team_role2 = $row["team_role2"];

    $team_image3 = $row["team_image3"];
    $team_name3 = $row["team_name3"];
    $team_role3 = $row["team_role3"];

    $gallery_img1 = $row["gallery_img1"];
    $gallery_img2 = $row["gallery_img2"];
    $gallery_img3 = $row["gallery_img3"];
    $gallery_img4 = $row["gallery_img4"];
    $gallery_img5 = $row["gallery_img5"];
    $gallery_img6 = $row["gallery_img6"];
    $gallery_img7 = $row["gallery_img7"];
    $gallery_img8 = $row["gallery_img8"];

    $happy_clients = $row["happy_clients"];
    $peoples_love = $row["peoples_love"];
    $gallery_description = $row["gallery_description"];
    $place = $row["place"];
    $zip = $row["zip"];
    $hours = $row["hours"];
    $phone = $row["phone"];
    $email = $row["email"];
    $address = $row["address"];

    
}

?>


<!DOCTYPE html>
<html lang="en">
<head>
  <!-- Meta -->
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="">
    <meta name="keywords" content="">
    <meta name="author" content="Awaiken Theme">
  <!-- Page Title -->
  <title><?php echo $title; ?></title>
  <!-- Google Fonts css-->
  <link href="https://fonts.googleapis.com/css?family=Merienda+One%7cMontserrat:300,400,500,600,700,800,900" rel="stylesheet">
  <!-- Bootstrap css -->
  <link href="../css2/bootstrap.min.css" rel="stylesheet" media="screen">
  <!-- Font Awesome icon css-->
  <link href="../css2/font-awesome.min.css" rel="stylesheet" media="screen">
  <link href="../css2/flaticon.css" rel="stylesheet" media="screen">
  <!-- Slick nav css -->
  <link rel="stylesheet" href="../css2/slicknav.css">
  <!-- Main custom css -->
  <link href="../css2/custom.css" rel="stylesheet" media="screen" >
  <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body data-spy="scroll" data-target="#navigation" data-offset="71">
  <!-- Preloader starts -->
  <div class="preloader">
    <div class="browser-screen-loading-content">
      <div class="loading-dots dark-gray">
        <img src="../images2/hearts.svg" alt="Preloader" />
      </div>
    </div>
    </div>
  <!-- Preloader Ends -->
  
  <!-- Header Section Starts-->
  <header>
    <nav id="main-nav" class="navbar navbar-default navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">
          <!-- Logo starts -->
          <a class="navbar-brand" href="#">
            <?php
            if(empty($logo))
            {
            ?>
            <h2 style="color: white;"><?php echo $logo_text; ?></h2>
            <?php
          }
          else
          {
            ?>
             <img src="../uploadedimages/<?php echo $logo;?>" alt="Logo" style="height:inherit;"/> 
          <?php
          }
           
           ?>
          </a>
          <!-- Logo Ends -->
          
          <!-- Responsive Menu button starts -->
          <div class="navbar-toggle">
          </div>
          <!-- Responsive Menu button Ends -->
        </div>
        
        <div id="responsive-menu"></div>
        
        <!-- Navigation starts -->
        <div class="navbar-collapse collapse" id="navigation">
          <ul class="nav navbar-nav navbar-right main-navigation" id="main-menu">
            <li class="active"><a href="#home">Home</a></li>
            <li><a href="#about">About</a></li>
            <li><a href="#service">Service</a></li>
            <li><a href="#ourteam">Our Team</a></li>
            <li><a href="#gallery">Gallery</a></li>
            <li><a href="#contact">Contact</a></li>
          </ul>
        </div>
        <!-- Navigation Ends -->
      </div>
    </nav>
  </header>
  <!-- Header Section Ends-->
  
  <!-- Banner Section Starts -->
  <div class="banner" id="home">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <div class="header-banner">
            <img src="../images2/logo.png" alt="Logo" />
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- Banner Section Ends -->
  
  <!-- About section starts -->
  <section class="aboutus" id="about">
    <div class="container">
      <div class="row">
        <div class="col-md-5">
          <div class="about-desc">
            <h2>We care about you at <span><?php echo $heading; ?></span></h2>
            
            <p><?php echo $about; ?></p>
            
            <a href="#" class="btn-custom">Join Now</a>
          </div>
        </div>
        
        <div class="col-md-7">
          <div class="about-image">
            <img src="../images2/about-img.png" alt="" />
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- About section ends -->
  
  <!-- What do you section starts -->
  <section class="what-do-you">
    <div class="container">
      <div class="row">
        <div class="col-md-3 col-sm-12">
          <div class="whatdoyou-heading">
            <h2>What do you need?</h2>
            <p><?php echo $caption; ?></p>
            
            <a href="#" class="btn-custom">Join Now</a>
          </div>
        </div>
        
        <div class="col-md-3 col-sm-4">
          <div class="whatdoyou-box">
            <figure>
              <img src="../uploadedimages/<?php echo $service_icon1;?>" alt="" />
            </figure>
            
            <div class="whatdoyou-body">
              <h3><?php echo $service_text1; ?></h3>
              <p><?php echo $service_description1; ?></p>
              <a href="#" class="btn-custom">Read More</a>
            </div>
          </div>
        </div>
        
        <div class="col-md-3 col-sm-4">
          <div class="whatdoyou-box">
            <figure>
              <img src="../uploadedimages/<?php echo $service_icon2;?>" alt="" />
            </figure>
            
            <div class="whatdoyou-body">
              <h3><?php echo $service_text2; ?></h3>
              <p><?php echo $service_description2; ?></p>
              <a href="#" class="btn-custom">Read More</a>
            </div>
          </div>
        </div>
        
        <div class="col-md-3 col-sm-4">
          <div class="whatdoyou-box">
            <figure>
              <img src="../uploadedimages/<?php echo $service_icon3;?>" alt="" />
            </figure>
            
            <div class="whatdoyou-body">
              <h3><?php echo $service_text3; ?></h3>
              <p><?php echo $service_description3; ?></p>
              <a href="#" class="btn-custom">Read More</a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- What do you section ends -->
  
  <!-- Services Section starts -->
  <section class="services" id="service">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <div class="main-title">
            <h2>We Specialize In</h2>
          </div>
        </div>
      </div>
      
      <div class="row">
        <div class="col-md-6 col-sm-6">
          <div class="service-box service-left">
            <div class="icon-box"><i class="flaticon-hand"></i></div>
            <h3><?php echo $feature_text1 ?></h3>
            <p><?php echo $feature_description1;?></p>
          </div>
        </div>
        
        <div class="col-md-6 col-sm-6">
          <div class="service-box service-right">
            <div class="icon-box"><i class="flaticon-lotus"></i></div>
            <h3><?php echo $feature_text2;?></h3>
            <p><?php echo $feature_description2;?></p>
          </div>
        </div>
        
        <div class="col-md-6 col-sm-6">
          <div class="service-box service-left">
            <div class="icon-box"><i class="flaticon-bowl"></i></div>
            <h3><?php echo $feature_text3; ?></h3>
            <p><?php echo $feature_description3; ?></p>
          </div>
        </div>
        
        <div class="col-md-6 col-sm-6">
          <div class="service-box service-right">
            <div class="icon-box"><i class="flaticon-essential-oil"></i></div>
            <h3><?php echo $feature_text4; ?></h3>
            <p><?php echo $feature_description4; ?></p>
          </div>
        </div>
        
        <div class="col-md-6 col-sm-6">
          <div class="service-box service-left">
            <div class="icon-box"><i class="flaticon-barber"></i></div>
            <h3><?php echo $feature_text5;?></h3>
            <p><?php echo $feature_description5;?></p>
          </div>
        </div>
        
        <div class="col-md-6 col-sm-6">
          <div class="service-box service-right">
            <div class="icon-box"><i class="flaticon-cosmetics"></i></div>
            <h3><?php echo $feature_text6;?></h3>
            <p><?php echo $feature_description6; ?></p>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- Services Section ends -->
  
  <!-- Membership section starts -->
  <!-- <section class="membership" id="pricing">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <div class="main-title">
            <h2>Membership Plan</h2>
          </div>
        </div>
      </div>
      
      <div class="row">
        <div class="col-md-3 col-sm-6">
          <div class="membership-box">
            <div class="membership-header">
              <h3><span>01</span>Start Care</h3>
              <h4>$100 /<span>Month</span></h4>
            </div>
            
            <div class="membership-body">
              <ul>
                <li>Nail Cutting</li>
                <li>Hair Styling</li>
                <li>Spa Therapy</li>
                <li>Hair Styling</li>
                <li>Oil Massage</li>
              </ul>
            </div>
            
            <div class="membership-footer">
              <a href="#" class="btn-custom">Become Member</a>
            </div>
          </div>
        </div>
        
        <div class="col-md-3 col-sm-6">
          <div class="membership-box">
            <div class="membership-header">
              <h3><span>02</span>Advanced</h3>
              <h4>$150 /<span>Month</span></h4>
            </div>
            
            <div class="membership-body">
              <ul>
                <li>Nail Cutting</li>
                <li>Hair Styling</li>
                <li>Spa Therapy</li>
                <li>Hair Styling</li>
                <li>Oil Massage</li>
              </ul>
            </div>
            
            <div class="membership-footer">
              <a href="#" class="btn-custom">Become Member</a>
            </div>
          </div>
        </div>
        
        <div class="col-md-3 col-sm-6">
          <div class="membership-box">
            <div class="membership-header">
              <h3><span>03</span>Full Service</h3>
              <h4>$200 /<span>Month</span></h4>
            </div>
            
            <div class="membership-body">
              <ul>
                <li>Nail Cutting</li>
                <li>Hair Styling</li>
                <li>Spa Therapy</li>
                <li>Hair Styling</li>
                <li>Oil Massage</li>
              </ul>
            </div>
            
            <div class="membership-footer">
              <a href="#" class="btn-custom">Become Member</a>
            </div>
          </div>
        </div>
        
        <div class="col-md-3 col-sm-6">
          <div class="membership-box">
            <div class="membership-header">
              <h3><span>04</span>All For VIP</h3>
              <h4>$500 /<span>Month</span></h4>
            </div>
            
            <div class="membership-body">
              <ul>
                <li>Nail Cutting</li>
                <li>Hair Styling</li>
                <li>Spa Therapy</li>
                <li>Hair Styling</li>
                <li>Oil Massage</li>
              </ul>
            </div>
            
            <div class="membership-footer">
              <a href="#" class="btn-custom">Become Member</a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section> -->
  <!-- Membership section ends -->
  
  <!-- Our Team Section starts -->
  <section class="ourteam" id="ourteam">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <div class="main-title">
            <h2>Our Experienced Team</h2>
          </div>
        </div>
      </div>
      
      <div class="row">
        <div class="col-md-4 col-sm-4">
          <div class="team-single">
            <figure>
              <img src="../uploadedimages/<?php echo $team_image1;?>" alt="" />
            </figure>
            
            <h3><?php echo $team_name1; ?></h3>
            <p><?php echo $team_role1;?></p>
          </div>
        </div>
        
        <div class="col-md-4 col-sm-4">
          <div class="team-single">
            <figure>
              <img src="../uploadedimages/<?php echo $team_image2;?>" alt="" />
            </figure>
            
            <h3><?php echo $team_name2; ?></h3>
            <p><?php echo $team_role2 ?></p>
          </div>
        </div>
        
        <div class="col-md-4 col-sm-4">
          <div class="team-single">
            <figure>
              <img src="../uploadedimages/<?php echo $team_image3;?>" alt="" />
            </figure>
            
            <h3><?php echo $team_name3;?></h3>
            <p><?php echo $team_role3;?></p>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- Our Team Section ends -->
  
  <!-- Photo Gallery section starts -->
  <section class="gallery" id="gallery">
    <div class="container-fluid">
      <div class="row no-pad">
        <div class="col-md-12">
          <div class="main-title">
            <h2>Photo Gallery</h2>
          </div>
        </div>
      </div>
      
      <div class="row no-pad">
        <div class="col-md-3 col-sm-4 col-xs-6">
          <div class="gallery-box">
            <figure>
              <img src="../uploadedimages/<?php echo $gallery_img1;?>" alt="" />
            </figure>
            
            <div class="gallery-overlay">
              <div class="gallery-info">
                <h3>Face Massage</h3>
                <p>Lorem Ispum Dummy Text</p>
              </div>
            </div>
          </div>
        </div>
        
        <div class="col-md-3 col-sm-4 col-xs-6">
          <div class="gallery-box">
            <figure>
              <img src="../uploadedimages/<?php echo $gallery_img2;?>" alt="" />
            </figure>
            
            <div class="gallery-overlay">
              <div class="gallery-info">
                <h3>Leg Massage</h3>
                <p>Lorem Ispum Dummy Text</p>
              </div>
            </div>
          </div>
        </div>
        
        <div class="col-md-3 col-sm-4 col-xs-6">
          <div class="gallery-box">
            <figure>
              <img src="../uploadedimages/<?php echo $gallery_img3;?>" alt="" />
            </figure>
            
            <div class="gallery-overlay">
              <div class="gallery-info">
                <h3>Body Massage</h3>
                <p>Lorem Ispum Dummy Text</p>
              </div>
            </div>
          </div>
        </div>
        
        <div class="col-md-3 col-sm-4 col-xs-6">
          <div class="gallery-box">
            <figure>
              <img src="../uploadedimages/<?php echo $gallery_img4;?>" alt="" />
            </figure>
            
            <div class="gallery-overlay">
              <div class="gallery-info">
                <h3>Classic Facial</h3>
                <p>Lorem Ispum Dummy Text</p>
              </div>
            </div>
          </div>
        </div>
        
        <div class="col-md-3 col-sm-4 col-xs-6">
          <div class="gallery-box">
            <figure>
              <img src="../uploadedimages/<?php echo $gallery_img5;?>" alt="" />
            </figure>
            
            <div class="gallery-overlay">
              <div class="gallery-info">
                <h3>Sea Salt Glow</h3>
                <p>Lorem Ispum Dummy Text</p>
              </div>
            </div>
          </div>
        </div>
        
        <div class="col-md-3 col-sm-4 col-xs-6">
          <div class="gallery-box">
            <figure>
              <img src="../uploadedimages/<?php echo $gallery_img6;?>" alt="" />
            </figure>
            
            <div class="gallery-overlay">
              <div class="gallery-info">
                <h3>Anti Aging Facial</h3>
                <p>Lorem Ispum Dummy Text</p>
              </div>
            </div>
          </div>
        </div>
        
        <div class="col-md-3 col-sm-4 col-xs-6">
          <div class="gallery-box">
            <figure>
              <img src="../uploadedimages/<?php echo $gallery_img7;?>" alt="" />
            </figure>
            
            <div class="gallery-overlay">
              <div class="gallery-info">
                <h3>Ginger Body Buff</h3>
                <p>Lorem Ispum Dummy Text</p>
              </div>
            </div>
          </div>
        </div>
        
        <div class="col-md-3 col-sm-4 col-xs-6">
          <div class="gallery-box">
            <figure>
              <img src="../uploadedimages/<?php echo $gallery_img8;?>" alt="" />
            </figure>
            
            <div class="gallery-overlay">
              <div class="gallery-info">
                <h3>Waxing</h3>
                <p>Lorem Ispum Dummy Text</p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- Photo Gallery section ends -->
  
  <!-- Contact section starts -->
  <section class="contact" id="contact">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <div class="main-title">
            <h2>Get in Touch</h2>
          </div>
        </div>
      </div>
      
      <div class="row">
        <div class="col-md-12">
          <div class="contact-box">
            <div class="contact-info">
              <p><i class="fa fa-mobile-phone"></i> <?php echo $phone;?></p>
              <p><i class="fa fa-map-marker"></i> <?php echo $address." ".$place." ".$zip;?></p>
              <p><i class="fa fa-clock-o"></i><?php echo $hours; ?></p>
            </div>
            
            <div class="contact-form">
              <form>
                <div class="form-group">
                  <input type="text" class="form-control" placeholder="ENTER YOUR NAME" />
                </div>
                
                <div class="form-group">
                  <input type="text" class="form-control" placeholder="ENTER YOUR EMAIL" />
                </div>
                
                <div class="form-group">
                  <input type="text" class="form-control" placeholder="ENTER YOUR MOBILE" />
                </div>
                
                <div class="form-group">
                  <textarea class="form-control" rows="6" placeholder="ENTER MESSAGE HERE"></textarea>
                </div>
                
                <div class="form-group">
                  <input type="submit" class="btn-custom" value="Submit" />
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- Contact section ends -->
  
  <!-- Footer section starts -->
  <footer>
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <div class="footer-logo">
            <a href="#">
              <i class="flaticon-lotus"></i>
            </a>
          </div>
            
          <div class="footer-social">
            <a href="#"><i class="fa fa-facebook"></i></a>
            <a href="#"><i class="fa fa-twitter"></i></a>
            <a href="#"><i class="fa fa-google-plus"></i></a>
            <a href="#"><i class="fa fa-linkedin"></i></a>
            <a href="#"><i class="fa fa-pinterest-p"></i></a>
          </div>
        </div>
      </div>
      
      <div class="row">
        <div class="col-md-6">
          <div class="footer-info">
            <p>Copyright &copy; Untitled. All rights reserved.<?php echo $sitename; ?></p>
          </div>
        </div>
        
        <div class="col-md-6">
          <div class="footer-menu">
            <ul>
              <li><a href="#">Home</a></li>
              <li><a href="#">About</a></li>
              <li><a href="#">Service</a></li>
            
              <li><a href="#">Gallery</a></li>
              <li><a href="#">Contact</a></li>
            </ul>
          </div>
        </div>
      </div>
    </div>
  </footer>
  <!-- Footer section ends -->
  
    <!-- Jquery Library File -->
  <script src="../js2/jquery-1.12.4.min.js"></script>
  <!-- Parallax -->
  <script src="../js2/jquery.parallax-1.1.3.js"></script>
  <!-- SmoothScroll -->
  <script src="../js2/SmoothScroll.js"></script>
    <!-- Bootstrap js file -->
  <script src="../js2/bootstrap.min.js"></script>
  <!-- Bootstrap form validator -->
  <script src="../js2/validator.min.js"></script>
    <!-- Counterup js file -->
  <script src="../js2/waypoints.min.js"></script>
    <script src="../js2/jquery.counterup.min.js"></script>
    <!-- Isotop js file -->
  <script src="../js2/isotope.min.js"></script>
    <!-- Slick Nav js file -->
  <script src="../js2/jquery.slicknav.js"></script>
  <!-- Owl Carousel js file -->
  <script src="../js2/owl.carousel.js"></script>
    <!-- Typed js file -->
  <script src="../js2/typed.js"></script>
  <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?libraries=places&key=AIzaSyAsdzx6REuLqvQJC9VffvqM_84hUlKbWyM"></script> 
    <!-- Main Custom js file -->
    <script src="../js2/function.js"></script>
</body>
</html>







<!-- <html>
<head><h1>VIEW SPAS </h1></head>
<body>
	<?php //echo $service_icon1; ?><br>
<img src="../uploadedimages/<?php //echo $service_icon1;?>" alt="img">
	
</body>
</html> -->