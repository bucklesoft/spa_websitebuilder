<?php
include_once('connection.php');
$url = $_SERVER['REQUEST_URI'];
if(strrchr($url, '/'))
{
    $str = substr(strrchr($url, '/'), 1);
}
else
{
    $str = strrchr($url, '/');
}

$qu = mysqli_query($con,"select * from spa_templates where sitename='$str'");
if($row = mysqli_fetch_assoc($qu))
{

$sitename = $row['sitename'];

if(isset($row["logo_text"]))
{
  $logo_text = $row["logo_text"];
}
if(isset($row["logo"]))
{
  $logo= $row["logo"];
}
$url = $row['url'];
$title = $row['title'];

$heading = $row['heading'];
$about =$row['about']; 
$caption = $row['caption'];

$services = $row['services'];
$service_text1 = $row['service_text1'];
$service_icon1 = $row['service_icon1'];
$service_description1 = $row['service_description1'];
$service_text2 = $row['service_text2'];
$service_icon2 = $row['service_icon2'];
$service_description2 = $row['service_description2'];

$features = $row['features'];
$feature_icon1 = $row['feature_icon1'];
$feature_text1 = $row['feature_text1'];
$feature_description1 = $row['feature_description1'];
$feature_icon2 = $row['feature_icon2'];
$feature_text2 = $row['feature_text2'];
$feature_description2 = $row['feature_description2'];
$feature_icon3 = $row['feature_icon3'];
$feature_text3 = $row['feature_text3'];
$feature_description3 = $row['feature_description3'];
$feature_icon4 = $row['feature_icon4'];
$feature_text4 = $row['feature_text4'];
$feature_description4 = $row['feature_description4'];
$feature_icon5 = $row['feature_icon5'];
$feature_text5 = $row['feature_text5'];
$feature_description5 = $row['feature_description5'];
$feature_icon6 = $row['feature_icon6'];
$feature_text6 = $row['feature_text6'];
$feature_description6 = $row['feature_description6'];

$testimonial_icon1 = $row['testimonial_icon1'];
$testimonial1 = $row['testimonial1'];
$testimonial_name1 = $row['testimonial_name1'];
$testimonial_icon2 = $row['testimonial_icon2'];
$testimonial2 = $row['testimonial2'];
$testimonial_name2 = $row['testimonial_name2'];
$testimonial_icon3 = $row['testimonial_icon3'];
$testimonial3 = $row['testimonial3'];
$testimonial_name3 = $row['testimonial_name3'];
$testimonial_icon4 = $row['testimonial_icon4'];
$testimonial4 = $row['testimonial4'];
$testimonial_name4 = $row['testimonial_name4'];

$happy_clients = $row['happy_clients'];
$peoples_love = $row['peoples_love'];

$gallery_description = $row['gallery_description'];
$gallery_img1 = $row['gallery_img1'];
$gallery_img2 = $row['gallery_img2'];
$gallery_img3 = $row['gallery_img3'];
$gallery_img4 = $row['gallery_img4'];
$gallery_img5 = $row['gallery_img5'];
$gallery_img6 = $row['gallery_img6'];

$address = $row['address'];
$place = $row['place'];
$zip = $row['zip'];
$hours = $row['hours'];
$phone = $row['phone'];
$email = $row['email'];


    
}

?>


<!DOCTYPE html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="keywords" content="Bootstrap, Parallax, Template, Registration, Landing">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="author" content="bromlays">
    <title><?php echo $title ?></title>
    <link rel="icon" href="img/classiclogo1.png">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="../css/bootstrap.min.css">
    <link rel="stylesheet" href="../css/font-awesome.min.css">
    <link rel="stylesheet" href="../css/line-icons.css">
    <link rel="stylesheet" href="../css/owl.carousel.css">
    <link rel="stylesheet" href="../css/owl.theme.css">
    <link rel="stylesheet" href="../css/nivo-lightbox.css">
    <link rel="stylesheet" href="../css/magnific-popup.css">
    <link rel="stylesheet" href="../css/slicknav.css">
    <link rel="stylesheet" href="../css/animate.css">
    <link rel="stylesheet" href="../css/main.css">    
    <link rel="stylesheet" href="../css/responsive.css">

  </head>
  <body>

    <!-- Header Section Start -->
    <header id="hero-area" data-stellar-background-ratio="0.5">    
      <!-- Navbar Start -->
      <nav class="navbar navbar-expand-lg fixed-top scrolling-navbar indigo">
        <div class="container">
          <!-- Brand and toggle get grouped for better mobile display -->
          <div class="navbar-header">
           

           <?php

            if(empty($logo))
            {
             
              ?>
<a href="./<?php echo $sitename; ?>" class="navbar-brand">
                <b><h6 style="color:#3d7929ba;font-size: 26px"><?php echo $logo_text;?></h6></b></a>

              
              <?php
              }
              else
              {
               
                ?>

              <a href="./<?php echo $sitename; ?>" class="navbar-brand"><img class="img-fulid" src="../uploadedimages/<?php echo $logo;?>" alt="Brand logo"></a>

                <?php
              }
            ?>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#main-navbar" aria-controls="main-navbar" aria-expanded="false" aria-label="Toggle navigation">
              <i class="lnr lnr-menu"></i>
            </button>
          </div>
          <div class="collapse navbar-collapse" id="main-navbar">
            <ul class="navbar-nav mr-auto w-100 justify-content-end">
              <li class="nav-item">
                <a class="nav-link page-scroll" href="#hero-area">Home</a>
              </li>
              <li  class="nav-item">
              <a href="#about" class="nav-link page-scroll" >About Us</a>
              </li>
              <li class="nav-item">
                <a class="nav-link page-scroll" href="#services">Services</a>
              </li>
              <li class="nav-item">
                <a class="nav-link page-scroll" href="#features">Features</a>
              </li>
              <!-- <li class="nav-item">
                <a class="nav-link page-scroll" href="#portfolios">Discover</a>
              </li> -->
              <li class="nav-item">
                <a class="nav-link page-scroll" href="#testimonial">Testimonials</a>
              </li>
               <li class="nav-item">
                <a class="nav-link page-scroll" href="#gallery">Gallery</a>
              </li>
              <!-- <li class="nav-item">
                <a class="nav-link page-scroll" href="#pricing">Membership</a>
              </li> -->
             <!--  <li class="nav-item">
                <a class="nav-link page-scroll" href="#team">Team</a>
              </li> -->
              <!-- <li class="nav-item">
                <a class="nav-link page-scroll" href="#blog">Blog</a>
              </li> -->
              
              <li class="nav-item">
                <a class="nav-link page-scroll" href="#contact">Contact</a>
              </li>
            </ul>
          </div>
        </div>

        <!-- Mobile Menu Start -->
        <ul class="mobile-menu">
           <li>
              <a class="page-scroll" href="#hero-area">Home</a>
            </li>
            <li>
              <a class="page-scroll" href="#services">Services</a>
            </li>
            <li>
              <a class="page-scroll" href="#features">Features</a>
            </li>
            <li>
              <a class="page-scroll" href="#portfolios">Discover</a>
            </li>
             <li>
              <a class="page-scroll" href="#testimonial">Testimonials</a>
            </li>
            <li>
              <a class="page-scroll" href="#gallery">Gallery</a>
            </li>
           <!--  <li>
              <a class="page-scroll" href="#pricing">Pricing</a>
            </li> -->
            <!-- <li>
              <a class="page-scroll" href="#team">Team</a>
            </li> -->
           <!--  <li >
              <a class="page-scroll" href="#blog">Blog</a>
            </li> -->
           
            <li>
              <a class="page-scroll" href="#contact">Contact</a>
            </li>
        </ul>
        <!-- Mobile Menu End -->

      </nav>
      <!-- Navbar End -->   
      <div class="container">      
        <div class="row justify-content-md-center">
          <div class="col-md-10">
            <div class="contents text-center">
              <h1 class="wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="0.3s"><?php echo $heading ?></h1>
              <p class="lead  wow fadeIn" data-wow-duration="1000ms" data-wow-delay="400ms"><?php echo $caption; ?>  </p>
              
            </div>
          </div>
        </div> 
      </div>           
    </header>
    <!-- Header Section End --> 

    <!--About Us Section Start-->


    <section id="about" class="section" name="about">
      <!-- Container Starts -->
      <div class="container">
        <div class="section-header">          
          <h2 class="section-title">About Us</h2>
          <hr class="lines">
          <p class="section-subtitle"></p>
        </div>
       <div class="aboutus">
        <p><?php echo $about; ?></p>
       </div>

        
          
          
      </div>
      <!-- Container Ends -->
    </section>



    


    <!--About Us Section End-->






    <!-- Services Section Start -->
    <section id="services" class="section">
      <div class="container">
        <div class="section-header">          
          <h2 class="section-title wow fadeIn" data-wow-duration="1000ms" data-wow-delay="0.3s">We Provide</h2>
          <hr class="lines wow zoomIn" data-wow-delay="0.3s">
          <p class="section-subtitle wow fadeIn" data-wow-duration="1000ms" data-wow-delay="0.3s"><?php echo $services ?>
        </div>
        <div class="row">
          <div class="col-md-6 col-sm-6">
            <div class="item-boxes wow fadeInDown" data-wow-delay="0.2s">
              <!-- <div class="icon">
                <i class="lnr lnr-pencil"></i>
              </div> -->
              <div>
                <img src="../uploadedimages/<?php echo $service_icon1;?>" alt="">
              </div>
              <br>
              <h4><?php echo $service_text1; ?></h4>
              <p><?php echo $service_description1; ?></p>
            </div>
          </div>
          <!-- <div class="col-md-4 col-sm-6">
            <div class="item-boxes wow fadeInDown" data-wow-delay="0.8s">
              <div>
                <img src="img/serum.png">
              </div>
              <br>
              <h4>BODY & HAIR CARE</h4>
              <p>Permanent solutions for your hair problems -Hair restoration, Hair and scalp treatments</p>
            </div>
          </div> -->
          <div class="col-md-6 col-sm-6">
            <div class="item-boxes wow fadeInDown" data-wow-delay="1.2s">
              <div>
                 <img src="../uploadedimages/<?php echo $service_icon2;?>" alt="">
              </div>
              <br>
              <h4><?php echo $service_text2 ?></h4>
              <p><?php echo $service_description2; ?></p>
            </div>
          </div>
        </div>
        <br><br>
          <center><div class="plan-button">
                <a href="#features" class="btn btn-common">View More</a>
          </div></center>
        
      </div>
    </section>
    <!-- Services Section End -->




    <!-- Features Section Start -->
    <section id="features" class="section">
      <div class="container">
        <div class="section-header">          
          <h2 class="section-title">Our Features</h2>
          <hr class="lines">
          <p class="section-subtitle"><?php echo $features; ?></p>
        </div>
        <div class="row">
          <div class="col-lg-12 col-md-12 col-xs-12">
            <div class="container p-b-50">
              <div class="row">
                 <div class="col-lg-4 col-sm-4 col-xs-12 box-item">
                    <div>
                       <img src="../uploadedimages/<?php echo $feature_icon1;?>" alt="">
                    </div>
                    <div class="text">
                      <h4><?php echo $feature_text1 ?></h4>
                      <p><?php echo $feature_description1; ?></p>
                    </div>
                  </div>
                  <div class="col-lg-4 col-sm-4 col-xs-12 box-item">
                    <div>
                       <img src="../uploadedimages/<?php echo $feature_icon2;?>" alt="">
                    </div>
                    <div class="text">
                      <h4><?php echo $feature_text2; ?></h4>
                      <p><?php echo $feature_description2; ?></p>
                    </div>
                  </div>
                  <div class="col-lg-4 col-sm-4 col-xs-12 box-item">
                    <div>
                       <img src="../uploadedimages/<?php echo $feature_icon3;?>" alt="">
                    </div>
                    <div class="text">
                      <h4><?php echo $feature_text3 ?></h4>
                      <p><?php echo $feature_description3 ?></p>
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-lg-4 col-sm-4 col-xs-12 box-item">
                    <div>
                       <img src="../uploadedimages/<?php echo $feature_icon4;?>" alt="">
                    </div>
                    <div class="text">
                      <h4><?php echo $feature_text4 ?></h4>
                      <p><?php echo $feature_description4 ?></p>
                    </div>
                  </div>
                  <div class="col-lg-4 col-sm-4 col-xs-12 box-item">
                    <div>
                       <img src="../uploadedimages/<?php echo $feature_icon5;?>" alt="">
                    </div>
                    <div class="text">
                      <h4><?php echo $feature_text5 ;?></h4>
                      <p><?php echo $feature_description5; ?></p>
                    </div>
                  </div>
                  <div class="col-lg-4 col-sm-4 col-xs-12 box-item">
                    <div>
                       <img src="../uploadedimages/<?php echo $feature_icon6;?>" alt="">
                    </div>
                    <div class="text">
                      <h4><?php echo $feature_text6 ?></h4>
                      <p><?php echo $feature_description6; ?></p>
                    </div>
                  </div>
              </div>
            </div>
          </div>
          
        </div>
      </div>
    </section>
    <!-- Features Section End -->    

    <!-- Portfolio Section -->
   <!-- <section id="portfolios" class="section">
      <!-- Container Starts -->
      <!--<div class="container">
        <div class="section-header">          
          <h2 class="section-title">DISCOVER OUR ALL NATURAL HEALTH SOLUTIONS FOR BODY AND SPIRIT</h2>
          <hr class="lines"></p> 
        </div>
        <div class="row">          
          <div class="col-md-12">
            <!-- Portfolio Controller/Buttons -->
            <!--<div class="controls text-center">
              <a class="filter active btn btn-common" data-filter="all">
                All 
              </a>
              <a class="filter btn btn-common" data-filter=".design">
                Spa & massage
              </a>
              <a class="filter btn btn-common" data-filter=".development">
                Body &hair care
              </a>
              <a class="filter btn btn-common" data-filter=".print">
                Home spa 
              </a>
            </div>
            <!-- Portfolio Controller/Buttons Ends-->
         <!-- </div>

          <!-- Portfolio Recent Projects -->
          <!--<div id="portfolio" class="row">
            <div class="col-sm-6 col-md-4 col-lg-4 col-xl-4 mix development print">
              <div class="portfolio-item">
                <div class="shot-item">
                  <img src="img/portfolio/img1.jpg" alt="" />  
                  <a class="overlay lightbox" href="img/portfolio/img1.jpg">
                    <i class="lnr lnr-eye item-icon"></i>
                  </a>
                </div>               
              </div>
            </div>
            <div class="col-sm-6 col-md-4 col-lg-4 col-xl-4 mix design print">
              <div class="portfolio-item">
                <div class="shot-item">
                  <img src="img/portfolio/img2.jpg" alt="" />  
                  <a class="overlay lightbox" href="img/portfolio/img2.jpg">
                    <i class="lnr lnr-eye item-icon"></i>
                  </a>
                </div>               
              </div>
            </div>
            <div class="col-sm-6 col-md-4 col-lg-4 col-xl-4 mix development">
              <div class="portfolio-item">
                <div class="shot-item">
                  <img src="img/portfolio/img3.jpg" alt="" />  
                  <a class="overlay lightbox" href="img/portfolio/img3.jpg">
                    <i class="lnr lnr-eye item-icon"></i>
                  </a>
                </div>               
              </div>
            </div>
            <div class="col-sm-6 col-md-4 col-lg-4 col-xl-4 mix development design">
              <div class="portfolio-item">
                <div class="shot-item">
                  <img src="img/portfolio/img4.jpg" alt="" />  
                  <a class="overlay lightbox" href="img/portfolio/img4.jpg">
                    <i class="lnr lnr-eye item-icon"></i>
                  </a>
                </div>               
              </div>
            </div>
            <div class="col-sm-6 col-md-4 col-lg-4 col-xl-4 mix development">
              <div class="portfolio-item">
                <div class="shot-item">
                  <img src="img/portfolio/img5.jpg" alt="" />  
                  <a class="overlay lightbox" href="img/portfolio/img5.jpg">
                    <i class="lnr lnr-eye item-icon"></i>
                  </a>
                </div>               
              </div>
            </div>
            <div class="col-sm-6 col-md-4 col-lg-4 col-xl-4 mix print design">
              <div class="portfolio-item">
                <div class="shot-item">
                  <img src="img/portfolio/img6.jpg" alt="" />  
                  <a class="overlay lightbox" href="img/portfolio/img6.jpg">
                    <i class="lnr lnr-eye item-icon"></i>
                  </a>
                </div>               
              </div>
            </div>
          </div>
        </div>
      </div>
      <!-- Container Ends -->
    <!--</section>-->
    <!-- Portfolio Section Ends --> 

    <!-- Start Video promo Section -->
    <!-- <section class="video-promo section">
      <div class="container">
        <div class="row justify-content-center">
          <div class="col-lg-8">
              <div class="video-promo-content text-center">
                <h2 class="wow zoomIn" data-wow-duration="1000ms" data-wow-delay="100ms">Watch Our Intro video</h2>
                <p class="wow zoomIn" data-wow-duration="1000ms" data-wow-delay="100ms">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
                <a href="https://www.youtube.com/watch?v=TihRobCDGQ0" class="video-popup wow fadeInUp" data-wow-duration="1000ms" data-wow-delay="0.3s"><i class="lnr lnr-film-play"></i></a>
              </div>
          </div>
        </div>
      </div>
    </section> -->
    <!-- End Video Promo Section -->

    <!-- Start Pricing Table Section -->
    <!-- <div id="pricing" class="section pricing-section">
      <div class="container">
        <div class="section-header">          
          <h2 class="section-title">Get A Membership</h2>
          <hr class="lines">
          <p class="section-subtitle">Get incredible value with our service packs. All members are eligible for regular discounts, apart from the seasonal offers and announcements. For every person reffered, you get 25% off on our Spa Package and Fitness Package.</p>
        </div>

        <div class="row pricing-tables">
          <div class="col-md-4 col-sm-6 col-xs-12">
            <div class="pricing-table">
              <div class="pricing-details">
                <h2>1 Month</h2>
                <span>$200 /mo</span>
                <p>Take advantage of our amazing membership plans. Long term clients benefit from additional offers!</p>
                <ul type="square">
                  <li>Free steam bath</li>
                  <li>Reflexotherapy</li>
                  <li>Aditional services</li>
                  <li>Valid for one</li>
                </ul>
              </div>
              <div class="plan-button">
                <a href="#" class="btn btn-common">Get Plan</a>
              </div>
            </div>
          </div>

          <div class="col-md-4 col-sm-6 col-xs-12">
            <div class="pricing-table">
              <div class="pricing-details">
                <h2>SIX MONTHS</h2>
                <span>$150 /mo</span>
                <p>Take advantage of our amazing membership plans. Long term clients benefit from additional offers!</p>
                <ul>
                  <li>Free steam bath</li>
                  <li>Reflexotherapy</li>
                  <li>Aditional services</li>
                  <li>Valid for one</li>
                </ul>
              </div>
              <div class="plan-button">
                <a href="#" class="btn btn-common">Get Plan</a>
              </div>
            </div>
          </div>

          <div class="col-md-4 col-sm-6 col-xs-12">
            <div class="pricing-table">
              <div class="pricing-details">
                <h2>ONE YEAR</h2>
                <span>$100 /mo</span>
                <p>Take advantage of our amazing membership plans. Long term clients benefit from additional offers!</p>
                <ul>
                  <li>Free steam bath</li>
                  <li>Reflexotherapy</li>
                  <li>Aditional services</li>
                  <li>Valid for one</li>
                </ul>
              </div>
              <div class="plan-button">
                <a href="#" class="btn btn-common">Get Plan</a>
              </div>
            </div>
          </div>

        </div>
      </div>
    </div> -->
    <!-- End Pricing Table Section -->

   

    <!-- Team section Start -->
    <!-- <section id="team" class="section">
      <div class="container">
        <div class="section-header">          
          <h2 class="section-title"> Meet Our Team</h2>
          <hr class="lines"> -->
          <!-- <p class="section-subtitle"> -->
          <!-- Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quaerat, dignissimos! <br> Lorem ipsum dolor sit amet, consectetur. -->
          <!-- </p> -->
       <!--  </div>
        <div class="row">
          <div class="col-lg-3 col-md-6 col-xs-12">
            <div class="single-team">
              <img src="img/team/team1.jpg" alt="">
              <div class="team-details">
                <div class="team-inner">
                  <h4 class="team-title">Joey</h4> -->
                  <!-- <p>Chief Technical Officer</p> -->
                 <!--  <ul class="social-list">
                    <li class="facebook"><a href="#"><i class="fa fa-facebook"></i></a></li>
                    <li class="twitter"><a href="#"><i class="fa fa-twitter"></i></a></li>
                    <li class="google-plus"><a href="#"><i class="fa fa-google-plus"></i></a></li>
                    <li class="linkedin"><a href="#"><i class="fa fa-linkedin"></i></a></li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
          <div class="col-lg-3 col-md-6 col-xs-12">
            <div class="single-team">
              <img src="img/team/team2.jpg" alt="">
              <div class="team-details">
                <div class="team-inner">
                  <h4 class="team-title">John</h4> -->
                  <!-- <p>CEO & Co-Founder</p> -->
                 <!--  <ul class="social-list">
                    <li class="facebook"><a href="#"><i class="fa fa-facebook"></i></a></li>
                    <li class="twitter"><a href="#"><i class="fa fa-twitter"></i></a></li>
                    <li class="google-plus"><a href="#"><i class="fa fa-google-plus"></i></a></li>
                    <li class="linkedin"><a href="#"><i class="fa fa-linkedin"></i></a></li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
          <div class="col-lg-3 col-md-6 col-xs-12">
            <div class="single-team">
              <img src="img/team/team3.jpg" alt="">
              <div class="team-details">
                <div class="team-inner">                  
                  <h4 class="team-title">Kim</h4> -->
                  <!-- <p>Business Manager</p> -->
                 <!--  <ul class="social-list">
                    <li class="facebook"><a href="#"><i class="fa fa-facebook"></i></a></li>
                    <li class="twitter"><a href="#"><i class="fa fa-twitter"></i></a></li>
                    <li class="google-plus"><a href="#"><i class="fa fa-google-plus"></i></a></li>
                    <li class="linkedin"><a href="#"><i class="fa fa-linkedin"></i></a></li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
          <div class="col-lg-3 col-md-6 col-xs-12">
            <div class="single-team">
              <img class="img-fulid" src="img/team/team4.jpg" alt="">
              <div class="team-details">
                <div class="team-inner">
                  <h4 class="team-title">Rapheal</h4> -->
                  <!-- <p>Graphic Designer</p> -->
                 <!--  <ul class="social-list">
                    <li class="facebook"><a href="#"><i class="fa fa-facebook"></i></a></li>
                    <li class="twitter"><a href="#"><i class="fa fa-twitter"></i></a></li>
                    <li class="google-plus"><a href="#"><i class="fa fa-google-plus"></i></a></li>
                    <li class="linkedin"><a href="#"><i class="fa fa-linkedin"></i></a></li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>  -->
    <!-- Team section End --> 

    <!-- testimonial Section Start -->
    <div id="testimonial" class="section" data-stellar-background-ratio="0.1">
      <div class="container">
        <div class="row justify-content-md-center">
          <div class="col-md-12">
            <div class="touch-slider owl-carousel owl-theme">
              <div class="testimonial-item">
                 <img src="../uploadedimages/<?php echo $testimonial_icon1;?>" alt="client testimonial">
                <div class="testimonial-text">
                  <p><?php echo $testimonial1; ?></p>
                  <h3><?php echo $testimonial_name1;?></h3>
                  
                </div>
              </div>
              <div class="testimonial-item">
                 <img src="../uploadedimages/<?php echo $testimonial_icon2;?>" alt="client testimonial">
                <div class="testimonial-text">
                  <p><?php echo $testimonial2; ?></p>
                  <h3><?php echo $testimonial_name2; ?></h3>
                  
                </div>
              </div>
              <div class="testimonial-item">
                 <img src="../uploadedimages/<?php echo $testimonial_icon3;?>" alt="client testimonial">
                <div class="testimonial-text">
                  <p><?php echo $testimonial3; ?></p>
                  <h3><?php echo $testimonial_name3; ?></h3>
                  
                </div>
              </div>
              <div class="testimonial-item">
                <img src="../uploadedimages/<?php echo $testimonial_icon4;?>" alt="client testimonial">
                <div class="testimonial-text">
                  <p><?php echo $testimonial3; ?></p>
                  <h3<?php echo $testimonial_name3; ?></h3>
                  
                </div>
              </div>
            </div>
          </div>
        </div>        
      </div>
    </div>
    <!-- testimonial Section Start -->

    <!-- Blog Section -->
  <!--   <section id="blog" class="section">
      
      <div class="container">
        <div class="section-header">          
          <h2 class="section-title">Recent Blog</h2>
          <hr class="lines">
          <p class="section-subtitle">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quaerat, dignissimos! <br> Lorem ipsum dolor sit amet, consectetur.</p>
        </div>
        <div class="row">
          <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 blog-item">
            
            <div class="blog-item-wrapper">
              <div class="blog-item-img">
                <a href="single-post.html">
                  <img src="img/blog/img1.jpg" alt="">
                </a>                
              </div>
              <div class="blog-item-text"> 
                <div class="meta-tags">
                  <span class="date"><i class="lnr  lnr-clock"></i>2 Days Ago</span>
                  <span class="comments"><a href="#"><i class="lnr lnr-bubble"></i> 24 Comments</a></span>
                </div>
                <h3>
                  <a href="single-post.html">Massage therapy is very effective at all ages</a>
                </h3>
                <p>
                Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua... 
                </p>
                <a href="single-post.html" class="btn-rm">Read More <i class="lnr lnr-arrow-right"></i></a>
              </div>
            </div>
            
          </div>

          <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 blog-item">
            
            <div class="blog-item-wrapper">
              <div class="blog-item-img">
                <a href="single-post.html">
                  <img src="img/blog/img2.jpg" alt="">
                </a>                
              </div>
              <div class="blog-item-text"> 
                <div class="meta-tags">
                  <span class="date"><i class="lnr  lnr-clock"></i>2 Days Ago</span>
                  <span class="comments"><a href="#"><i class="lnr lnr-bubble"></i> 24 Comments</a></span>
                </div>
                <h3>
                  <a href="single-post.html">Lower your stress with aroma therapy</a>
                </h3>
                <p>
                Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua... 
                </p>
                <a href="single-post.html" class="btn-rm">Read More <i class="lnr lnr-arrow-right"></i></a>
              </div>
            </div>
           
          </div>

          <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 blog-item">
            
            <div class="blog-item-wrapper">
              <div class="blog-item-img">
                <a href="single-post.html">
                  <img src="img/blog/img3.jpg" alt="">
                </a>                
              </div>
              <div class="blog-item-text"> 
                <div class="meta-tags">
                  <span class="date"><i class="lnr  lnr-clock"></i>2 Days Ago</span>
                  <span class="comments"><a href="#"><i class="lnr lnr-bubble"></i> 24 Comments</a></span>
                </div> 
                <h3>
                  <a href="single-post.html">Top 5 therapies to try when you visit a Spa</a>
                </h3>
                <p>
                 Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua... 
                </p>
                <a href="single-post.html" class="btn-rm">Read More <i class="lnr lnr-arrow-right"></i></a>
              </div>
            </div>
            
          </div>
        </div>
      </div>
    </section> -->
    <!-- blog Section End -->

     <!-- Counter Section Start -->
    <div class="counters section" data-stellar-background-ratio="0.5">
      <div class="container">
        <div class="row"> 
          <div class="col-sm-6 col-md-3 col-lg-3">
            <div class="facts-item">   
              <div class="icon">
                <i class="lnr lnr-user"></i>
              </div>             
              <div class="fact-count">
                <h3><span class="counter"><?php echo $happy_clients; ?></span></h3>
                <h4>Happy Clients</h4>
              </div>
            </div>
          </div>
          <div class="col-sm-6 col-md-3 col-lg-3">
            <div class="facts-item">   
              <!-- <div class="icon">
                <i class="lnr lnr-briefcase"></i>
              </div> -->            
              <div class="fact-count">
                <!-- <h3><span class="counter">699</span></h3>
                <h4>Completed Projects</h4> -->
              </div>
            </div>
          </div>
          <div class="col-sm-6 col-md-3 col-lg-3">
            <div class="facts-item"> 
              <div class="icon">
                <i class="lnr lnr-heart"></i>
              </div>              
              <div class="fact-count">
                <h3><span class="counter"><?php echo $peoples_love; ?></span></h3>
                <h4>Peoples Love</h4>
              </div>
            </div>
          </div>
          <div class="col-sm-6 col-md-3 col-lg-3">
            <div class="facts-item"> 
              <!-- <div class="icon">
                <i class="lnr lnr-heart"></i>
              </div>    -->           
              <div class="fact-count">
                <!-- <h3><span class="counter">1689</span></h3>
                <h4>Peoples Love</h4> -->
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- Counter Section End -->


    <!--Gallery section start-->
     <section id="gallery" class="section">
      <!-- Container Starts -->
      <div class="container">
        <div class="section-header">          
          <h2 class="section-title">CHECKOUT OUR GALLERY</h2>
          <hr class="lines">
          <p class="section-subtitle"><?php echo $gallery_description; ?></p>
        </div>
       

          <!-- Gallery images-->
          <div id="gallery" class="row">
            <div class="col-sm-6 col-md-4 col-lg-4 col-xl-4 mix">
              <div class="gallery-item">
                <div class="shot-item">
                  <img src="../uploadedimages/<?php echo $gallery_img1;?>" alt="" height="251px" width="335px" />  
                  <a class="overlay lightbox" href="../uploadedimages/<?php echo $gallery_img1;?>">
                    <i class="lnr lnr-eye item-icon"></i>
                  </a>
                </div>               
              </div>
            </div>
            <div class="col-sm-6 col-md-4 col-lg-4 col-xl-4 mix">
              <div class="gallery-item">
                <div class="shot-item">
                  <img src="../uploadedimages/<?php echo $gallery_img2;?>" alt="" height="251px" width="335px" />   
                  <a class="overlay lightbox" href="../uploadedimages/<?php echo $gallery_img2;?>">
                    <i class="lnr lnr-eye item-icon"></i>
                  </a>
                </div>               
              </div>
            </div>
            <div class="col-sm-6 col-md-4 col-lg-4 col-xl-4 mix">
              <div class="gallery-item">
                <div class="shot-item">
                  <img src="../uploadedimages/<?php echo $gallery_img3;?>" alt="" height="251px" width="335px" />  
                  <a class="overlay lightbox" href="../uploadedimages/<?php echo $gallery_img3;?>">
                    <i class="lnr lnr-eye item-icon"></i>
                  </a>
                </div>               
              </div>
            </div>
            
              <div class="col-sm-6 col-md-4 col-lg-4 col-xl-4 mix">
              <div class="gallery-item">
                <div class="shot-item">
                  <img src="../uploadedimages/<?php echo $gallery_img4;?>" alt="image 4" height="251px" width="335px" />  
                  <a class="overlay lightbox" href="../uploadedimages/<?php echo $gallery_img4;?>">
                    <i class="lnr lnr-eye item-icon"></i>
                  </a>
                </div>               
              </div>
            </div>

            <div class="col-sm-6 col-md-4 col-lg-4 col-xl-4 mix">
              <div class="gallery-item">
                <div class="shot-item">
                  <img src="../uploadedimages/<?php echo $gallery_img5;?>" alt="" height="251px" width="335px" />  
                  <a class="overlay lightbox" href="../uploadedimages/<?php echo $gallery_img5;?>">
                    <i class="lnr lnr-eye item-icon"></i>
                  </a>
                </div>               
              </div>
            </div>

            <div class="col-sm-6 col-md-4 col-lg-4 col-xl-4 mix">
              <div class="gallery-item">
                <div class="shot-item">
                  <img src="../uploadedimages/<?php echo $gallery_img6;?>" alt="" height="251px" width="335px" />  
                  <a class="overlay lightbox" href="../uploadedimages/<?php echo $gallery_img6;?>">
                    <i class="lnr lnr-eye item-icon"></i>
                  </a>
                </div>               
              </div>
            </div>

            <!-- <div class="col-sm-6 col-md-4 col-lg-4 col-xl-4 mix">
              <div class="gallery-item">
                <div class="shot-item">
                  <img src="img/gallery/spa3.jpg" alt="" height="251px" width="335px" />  
                  <a class="overlay lightbox" href="img/gallery/gal5.jpeg">
                    <i class="lnr lnr-eye item-icon"></i>
                  </a>
                </div>               
              </div>
            </div>

            <div class="col-sm-6 col-md-4 col-lg-4 col-xl-4 mix">
              <div class="gallery-item">
                <div class="shot-item">
                  <img src="img/gallery/spa2.jpg" alt="" height="251px" width="335px" />  
                  <a class="overlay lightbox" href="img/gallery/gal5.jpeg">
                    <i class="lnr lnr-eye item-icon"></i>
                  </a>
                </div>               
              </div>
            </div>

            <div class="col-sm-6 col-md-4 col-lg-4 col-xl-4 mix">
              <div class="gallery-item">
                <div class="shot-item">
                  <img src="img/gallery/spa1.jpg" alt="" height="251px" width="335px" />  
                  <a class="overlay lightbox" href="img/gallery/gal5.jpeg">
                    <i class="lnr lnr-eye item-icon"></i>
                  </a>
                </div>               
              </div>
            </div>
           -->
           
          </div>
        </div>
      </div>
      <!-- Container Ends -->
    </section>




    <!-- Gallery section End-->




    <!-- Contact Section Start -->
    <section id="contact" class="section" data-stellar-background-ratio="-0.2">      
      <div class="contact-form">
        <div class="container">
          <div class="row">     
            <div class="col-lg-6 col-sm-6 col-xs-12">
              <div class="contact-us">
                <h3>Contact Us</h3>
                <div class="contact-address">
                  <p><?php echo $address ?> </p>
                  <p><?php echo $place.",".$zip; ?></p>
                  <p><?php echo $hours ?></p>
                  <p class="phone">Phone: <?php echo $phone; ?></p>
                  <p class="email">E-mail: <?php echo $email; ?></p>
                </div>
                <!-- <div class="social-icons">
                  <ul>
                    <li class="facebook"><a href="#"><i class="fa fa-facebook"></i></a></li>
                    <li class="twitter"><a href="#"><i class="fa fa-twitter"></i></a></li>
                    <li class="google-plus"><a href="#"><i class="fa fa-google-plus"></i></a></li>
                    <li class="linkedin"><a href="#"><i class="fa fa-linkedin"></i></a></li>
                    <li class="dribbble"><a href="#"><i class="fa fa-dribbble"></i></a></li>
                  </ul>
                </div> -->
              </div>
            </div>   
              
            <div class="col-lg-6 col-sm-6 col-xs-12">
              <div class="contact-block">
                <form id="contactForm" name="f1" method="post" action="form-process.php"  onsubmit="return checkform(this);"
>
                  <div class="row">
                    <div class="col-md-12">
                      <div class="form-group">
                        <input type="text" class="form-control" id="name" name="name" placeholder="Your Name" required data-error="Please enter your name">
                        <div class="help-block with-errors"></div>
                      </div>                                 
                    </div>
                    <div class="col-md-12">
                      <div class="form-group">
                        <input type="text" placeholder="Your Email" id="email" class="form-control" name="email" required data-error="Please enter your email">
                        <div class="help-block with-errors"></div>
                      </div> 
                    </div>
                    <div class="col-md-12">
                      <div class="form-group"> 
                        <textarea class="form-control" id="message" placeholder="Your Message" rows="8" data-error="Write your message" name="message" required></textarea>
                        <div class="help-block with-errors"></div>
                      </div>
                      <!-- START CAPTCHA -->
<br>
<div class="capbox">

<div id="CaptchaDiv"></div>

<div class="capbox-inner">
Type the above number:<br>

<input type="hidden" id="txtCaptcha">
<input type="text" name="CaptchaInput" id="CaptchaInput" size="15"><br>

</div>
</div>
<br><br>
<!-- END CAPTCHA -->

                      <div class="submit-button text-center">
                        <button class="btn btn-common1" id="submit" type="submit">Send Message</button>
                        <div id="msgSubmit" class="h3 text-center hidden"></div> 
                        <div class="clearfix"></div> 
                      </div>
                    </div>
                  </div>            
                </form>
                <script type="text/javascript">

                  // Captcha Script

                  function checkform(theform){
                  var why = "";

                  if(theform.CaptchaInput.value == ""){
                  why += "- Please Enter CAPTCHA Code.\n";
                  }
                  if(theform.CaptchaInput.value != ""){
                  if(ValidCaptcha(theform.CaptchaInput.value) == false){
                  why += "- The CAPTCHA Code Does Not Match.\n";
                  }
                  }
                  if(why != ""){
                  alert(why);
                  return false;
                  }
                  }

                  var a = Math.ceil(Math.random() * 9)+ '';
                  var b = Math.ceil(Math.random() * 9)+ '';
                  var c = Math.ceil(Math.random() * 9)+ '';
                  var d = Math.ceil(Math.random() * 9)+ '';
                  var e = Math.ceil(Math.random() * 9)+ '';

                  var code = a + b + c + d + e;
                  document.getElementById("txtCaptcha").value = code;
                  document.getElementById("CaptchaDiv").innerHTML = code;

                  // Validate input against the generated number
                  function ValidCaptcha(){
                  var str1 = removeSpaces(document.getElementById('txtCaptcha').value);
                  var str2 = removeSpaces(document.getElementById('CaptchaInput').value);
                  if (str1 == str2){
                  return true;
                  }else{
                  return false;
                  }
                  }
                  
                  // Remove the spaces from the entered and generated code
                  function removeSpaces(string){
                  return string.split(' ').join('');
                  }
                  </script>

              </div>
            </div>
          </div>
        </div>
      </div>           
    </section>
    <!-- Contact Section End -->

    <!-- Footer Section Start -->
    <footer>          
      <div class="container">
        <div class="row">
          <!-- Footer Links -->
          <div class="col-lg-6 col-sm-6 col-xs-12">
            <ul class="footer-links">
              <li>
                <a href="#">Homepage</a>
              </li>
              <li>
                <a href="#">Services</a>
              </li>
              <li>
                <a href="#">About Us</a>
              </li>
              <li>
                <a href="#">Contact</a>
              </li>
            </ul>
          </div>
          <div class="col-lg-6 col-sm-6 col-xs-12">
            <div class="copyright">
              <p>All copyrights reserved &copy; 2018</p>
            </div>
          </div>  
        </div>
      </div>
    </footer>
    <!-- Footer Section End --> 

    <!-- Go To Top Link -->
    <a href="#" class="back-to-top">
      <i class="lnr lnr-arrow-up"></i>
    </a>
    
    <div id="loader">
      <div class="spinner">
        <div class="double-bounce1"></div>
        <div class="double-bounce2"></div>
      </div>
    </div>     

    <!-- jQuery first, then Tether, then Bootstrap JS. -->
    <script src="../js/jquery-min.js"></script>
    <script src="../js/popper.min.js"></script>
    <script src="../js/bootstrap.min.js"></script>
    <script src="../js/jquery.mixitup.js"></script>
    <script src="../js/nivo-lightbox.js"></script>
    <script src="../js/owl.carousel.js"></script>    
    <script src="../js/jquery.stellar.min.js"></script>    
    <script src="../js/jquery.nav.js"></script>    
    <script src="../js/scrolling-nav.js"></script>    
    <script src="../js/jquery.easing.min.js"></script>    
    <script src="../js/smoothscroll.js"></script>    
    <script src="../js/jquery.slicknav.js"></script>     
    <script src="../js/wow.js"></script>   
    <script src="../js/jquery.vide.js"></script>
    <script src="../js/jquery.counterup.min.js"></script>    
    <script src="../js/jquery.magnific-popup.min.js"></script>    
    <script src="../js/waypoints.min.js"></script>    
    <script src="../js/form-validator.min.js"></script>
    <script src="../js/contact-form-script.js"></script>   
    <script src="../js/main.js"></script>

  </body>
</html>







<!-- <html>
<head><h1>VIEW SPAS </h1></head>
<body>
	<?php //echo $service_icon1; ?><br>
<img src="../uploadedimages/<?php //echo $service_icon1;?>" alt="img">
	
</body>
</html> -->