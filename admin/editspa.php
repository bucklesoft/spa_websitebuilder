<?php
include_once('connection.php');
$id = $_GET['id'];
$theme_name=$_GET['theme_name'];
$qu=mysqli_query($con,"select * from spa_templates where id ='$id' and theme_name='$theme_name'");
while($row = mysqli_fetch_array($qu))
{
    $id = $row["id"];
    $sitename = $row["sitename"];
    $logo = $row["logo"];
    $url = $row["url"];
    $title = $row["title"];
    $logo_text=$row["logo_text"];
    $heading = $row["heading"];
    $caption = $row["caption"];
    $about = $row["about"];
    $services = $row["services"];
    $service_icon1 = $row["service_icon1"];
    $service_text1 = $row["service_text1"];
    $service_description1 = $row["service_description1"];
    $service_icon2 = $row["service_icon2"];
    $service_text2 = $row["service_text2"];
    $service_description2 = $row["service_description2"];
    $service_icon3 = $row["service_icon3"];
    $service_text3 = $row["service_text3"];
    $service_description3 = $row["service_description3"];
    $features = $row["features"];

    $feature_text1 = $row["feature_text1"];
    $feature_icon1 = $row["feature_icon1"];
    $feature_description1 = $row["feature_description1"];
   
    $feature_text2 = $row["feature_text2"];
    $feature_icon2 = $row["feature_icon2"];
    $feature_description2 = $row["feature_description2"];

    $feature_text3 = $row["feature_text3"];
    $feature_icon3 = $row["feature_icon3"];
    $feature_description3 = $row["feature_description3"];

    $feature_text4 = $row["feature_text4"];
    $feature_icon4 = $row["feature_icon4"];
    $feature_description4 = $row["feature_description4"];

    $feature_text5 = $row["feature_text5"];
    $feature_icon5 = $row["feature_icon5"];
    $feature_description5 = $row["feature_description5"];

    $feature_text6 = $row["feature_text6"];
    $feature_icon6 = $row["feature_icon6"];
    $feature_description6 = $row["feature_description6"];

    $testimonial_icon1 = $row["testimonial_icon1"];
    $testimonial1 = $row["testimonial1"];
    $testimonial_name1 = $row["testimonial_name1"];

    $testimonial_icon2 = $row["testimonial_icon2"];
    $testimonial2 = $row["testimonial2"];
    $testimonial_name2 = $row["testimonial_name2"];

    $testimonial_icon3 = $row["testimonial_icon3"];
    $testimonial3 = $row["testimonial3"];
    $testimonial_name3 = $row["testimonial_name3"];

    $testimonial_icon4 = $row["testimonial_icon4"];
    $testimonial4 = $row["testimonial4"];
    $testimonial_name4 = $row["testimonial_name4"];


    $team_image1 = $row["team_image1"];
    $team_name1 = $row["team_name1"];
    $team_role1 = $row["team_role1"];

    $team_image2 = $row["team_image2"];
    $team_name2 = $row["team_name2"];
    $team_role2 = $row["team_role2"];

    $team_image3 = $row["team_image3"];
    $team_name3 = $row["team_name3"];
    $team_role3 = $row["team_role3"];

    $gallery_img1 = $row["gallery_img1"];
    $gallery_img2 = $row["gallery_img2"];
    $gallery_img3 = $row["gallery_img3"];
    $gallery_img4 = $row["gallery_img4"];
    $gallery_img5 = $row["gallery_img5"];
    $gallery_img6 = $row["gallery_img6"];
    $gallery_img6 = $row["gallery_img7"];
    $gallery_img6 = $row["gallery_img8"];

    $happy_clients = $row["happy_clients"];
    $peoples_love = $row["peoples_love"];
    $gallery_description = $row["gallery_description"];
    $place = $row["place"];
    $zip = $row["zip"];
    $hours = $row["hours"];
    $phone = $row["phone"];
    $email = $row["email"];
    $address = $row["address"];

    $theme=$row["theme_name"];

}

?>
<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Required meta tags-->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Colorlib Templates">
    <meta name="author" content="Colorlib">
    <meta name="keywords" content="Colorlib Templates">

    <!-- Title Page-->
    <title>Form</title>

    <!-- Icons font CSS-->
    <link href="../vendor1/mdi-font/css/material-design-iconic-font.min.css" rel="stylesheet" media="all">
    <link href="vendor1/font-awesome-4.7/css/font-awesome.min.css" rel="stylesheet" media="all">
    <!-- Font special for pages-->
    <link href="https://fonts.googleapis.com/css?family=Poppins:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

    <!-- Vendor CSS-->
    <link href="../vendor1/select2/select2.min.css" rel="stylesheet" media="all">
    <link href="../vendor1/datepicker/daterangepicker.css" rel="stylesheet" media="all">

    <!-- Main CSS-->
    <link href="../css1/main.css" rel="stylesheet" media="all">
</head>

<body>
    <div class="page-wrapper bg-gra-02 p-t-130 p-b-100 font-poppins">
        <div class="wrapper wrapper--w680">
            <div class="card card-4">
                <div class="card-body">
                	<h1 class="title">Basic Information</h1>
                    <form name="f1" method="post"  action = "updatespa.php" enctype="multipart/form-data">
                    	<div id="messages"></div>

                       <!--  <div class="row row-space">
                            <div class="col-2">
                                Select Theme:
                                <select class="input-group" name="theme">
                                   <option value="1" <?php if ($theme == '1') echo 'selected="selected"';?>>1</option>
    <option value="2"<?php if ($theme == '2') echo ' selected="selected"'; ?>>2</option>
                                    <option value="Theme2">Theme2</option>
                                </select>
                            </div>
                        </div> -->
                       
                        <div class="row row-space">
                            <div class="col-2">
                                <div class="input-group ">
                                    <label class="label required">Site Name</label>
                                    <input class="input--style-4" type="text" name="sitename" value="<?php echo $sitename;?>">
                                </div>
                            </div>
                            <div class="col-2">
                                <div class="input-group">
                                    <label class="label">URL</label>
                                    <input class="input--style-4" type="text" name="url" value="<?php echo $url;?>">
                                </div>
                            </div>
                        </div>
                        <div class = "row row-space">
                            <div class="col">
                                <div class="input-group ">
                                    <label class="label required">Title</label>
                                    <input class="input--style-4" type="text" name="title" id="title" size=100 style= 
                                    "width:100%" value="<?php echo $title;?>">
                                </div>
                            </div>
                        </div>
                        <div class = "row row-space">
                            <div class="col-2">
                                <div class="input-group ">
                                    <label class="label required">Logo</label>
                                    <img src="../uploadedimages/<?php echo $logo;?>" alt = "<?php echo $logo; ?>" width="120px" height="50px">
                                    <input class="input--style-4" type="file" name="image" id="image">
                                </div>
                            </div>
                            <div class="col-2">
                                <div class="input-group ">
                                    <label class="label required">Logo Text</label>
                                    <input class="input--style-4" type="text" name="logo_text" id="logo_text"  value="<?php echo $logo_text;?>">
                                </div>
                            </div>
                        </div>
                       
                       
                        
                        
                    
                    <h1 class="title border">Banner Information</h1>
                    
                        <div class="row row-space">
                            <div class="col">
                                <div class="input-group">
                                    <label class="label">Heading</label>
                                    <input class="input--style-4" type="text" name="heading" value="<?php echo $heading;?>" size="100%">
                                </div>
                            </div>
                        </div>
                        <div class="row row-space">
                            <div class="col">
                                <div class="input-group">
                                    <label class="label">Caption</label>
                                    <input class="input--style-4" type="text" name="caption" value="<?php echo $caption;?>" size="100%">
                                </div>
                            </div>
                           
                        </div>
                         <h1 class="title border">About Information</h1>
                    
                        <div class="row row-space">
                           <!--  <div class="col">
                                <div class="input-group">
                                    <label class="label">About</label>
                                    <textarea rows="5" cols="20" name="about" ><?php  $about;?></textarea>
                                </div>
                            </div>
                            -->
                            <div class="col">
                                <div class="input-group">
                                    <label class="label">About</label>
                                    <textarea rows = "5" cols="100%" name = "about"  class="input--style-4"><?php echo $about;?></textarea>
                                </div>
                            </div>
                           
                        </div>
                      
                       
                    <h1 class="title border">Service Information</h1>
                    
                        <div class="row row-space">
                            <div class="col">
                                <div class="input-group">
                                    <label class="label">Service Intro</label>

                                    <textarea rows = "3" cols="100%" name = "services"  class="input--style-4"><?php echo $services;?></textarea>
                                </div>
                            </div>
                            
                        </div>
                             <div class="row row-space">

                            <div class="col-2">
                                <div class="input-group">
                                    <label class="label">Service Icon 1</label>
                                    <img src="../uploadedimages/<?php echo $service_icon1;?>" alt="<?php echo $service_icon1; ?>" height="50px" width="100px">
                                    <input  class="input--style-4" type="file" name="service_icon1" value="<?php echo $service_icon1;?>">
                                </div>
                            </div>
                            <div class="col-2">
                                <div class="input-group">
                                    <label class="label">Service Name 1</label>
                                    <input class="input--style-4" type="text" name="service_text1" value="<?php echo $service_text1;?>">
                                </div>
                            </div>
                        </div>
                           <div class="row row-space">
                     
                        <div class="col">
                                <div class="input-group">
                                    <label class="label">Service Description 1</label>
                                    <textarea rows = "2" cols = "100%" name = "service_description1"  class="input--style-4"><?php echo $service_description1;?></textarea>
                                </div>
                            </div>
                               </div>
                           <div class="row row-space">

                            <div class="col-2">
                                <div class="input-group">
                                    <label class="label">Service Icon 2</label>
                                    <img src="../uploadedimages/<?php echo $service_icon2?>" alt="<?php echo $service_icon2; ?>" height="50px" width="100px">
                                    <input class="input--style-4" type="file" name="service_icon2" value="<?php echo $service_icon2;?>">
                                </div>
                            </div>
                            <div class="col-2">
                                <div class="input-group">
                                    <label class="label">Service Name 2</label>
                                    <input class="input--style-4" type="text" name="service_text2" value="<?php echo $service_text2;?>">
                                </div>
                            </div>
                     </div>
                     <div class="row row-space">
                        <div class="col-3">
                                <div class="input-group">
                                    <label class="label">Service Description 2</label>
                                    <textarea rows = "2" cols = "100%" name = "service_description2" class="input--style-4" ><?php echo $service_description2;?></textarea>
                                </div>
                            </div>
                               </div>

                               <?php

                               if($theme=="2")
                               {
                                ?>

                               <div class="row row-space">

                            <div class="col-2">
                                <div class="input-group">
                                    <label class="label">Service Icon 3</label>
                                    <img src="../uploadedimages/<?php echo $service_icon3?>" alt="<?php echo $service_icon3; ?>" height="50px" width="100px">
                                    <input class="input--style-4" type="file" name="service_icon3" value="<?php echo $service_icon3;?>">
                                </div>
                            </div>
                            <div class="col-2">
                                <div class="input-group">
                                    <label class="label">Service Name 3</label>
                                    <input class="input--style-4" type="text" name="service_text3" value="<?php echo $service_text3;?>">
                                </div>
                            </div>
                     </div>
                     <div class="row row-space">
                        <div class="col-3">
                                <div class="input-group">
                                    <label class="label">Service Description 3</label>
                                    <textarea rows = "2" cols = "100%" name = "service_description3" class="input--style-4" ><?php echo $service_description3;?></textarea>
                                </div>
                            </div>
                               </div>
                               <?php
                           }
                           ?>

                               <h1 class="title border">Feature Information</h1>
                    
                        <div class="row row-space">
                            <div class="col">
                                <div class="input-group">
                                    <label class="label">Feature Intro</label>
                                    <textarea rows = "3" cols="100%" name = "features"  class="input--style-4"><?php echo $features;?></textarea>
                                </div>
                            </div>
                            
                        </div>
                             <div class="row row-space">

                            <div class="col-2">
                                <div class="input-group">
                                    <label class="label">Feature Icon 1</label>
                                    <img src="../uploadedimages/<?php echo $feature_icon1;?>" width="50px" height="50px">
                                    <input class="input--style-4" type="file" name="feature_icon1">
                                </div>
                            </div>
                            <div class="col-2">
                                <div class="input-group">
                                    <label class="label">Feature Name 1</label>
                                    <input class="input--style-4" type="text" name="feature_text1" value="<?php echo $feature_text1;?>">
                                </div>
                            </div>
                        </div>
                        <div class="row row-space">
                     
                        <div class="col">
                        <div class="input-group">
                                    <label class="label">Feature Description 1</label>
                                    <div class="input-group">
                                    <label class="label"></label>
                                    <textarea rows="2" cols="100%" name="feature_description1" class="input--style-4"  ><?php echo $feature_description1;?></textarea>
                                </div>
                                </div>
                            </div>
                               </div>
                           <div class="row row-space">

                            <div class="col-2">
                                <div class="input-group">
                                    <label class="label">Feature Icon 2</label>
                                    <img src="../uploadedimages/<?php echo $feature_icon2;?>" width="50px" height="50px">
                                    <input class="input--style-4" type="file" name="feature_icon2">
                                </div>
                            </div>
                            <div class="col-2">
                                <div class="input-group">
                                    <label class="label">Feature Name 2</label>
                                    <input class="input--style-4" type="text" name="feature_text2"  value="<?php echo $feature_text2;?>">
                                </div>
                            </div>
                     </div>
                     <div class="row row-space">
                        <div class="col">
                        <div class="input-group">
                                    <label class="label">Feature Description 2</label>
                                    <textarea rows = "2" cols = "100%" name = "feature_description2"  class="input--style-4"><?php echo $feature_description2;?></textarea>
                                </div>
                            </div>
                            <div class="row row-space">

                            <div class="col-2">
                                <div class="input-group">
                                    <label class="label">Feature Icon 3</label>
                                    <img src="../uploadedimages/<?php echo $feature_icon3;?>" width="50px" height="50px">
                                    <input class="input--style-4" type="file" name="feature_icon3" size="50%">
                                </div>
                            </div>
                            <div class="col-2">
                                <div class="input-group">
                                    <label class="label">Feature Name 3</label>
                                    <input class="input--style-4" type="text" name="feature_text3" size="50%" value="<?php echo $feature_text3;?>" >
                                </div>
                            </div>
                        </div>
                        <div class="row row-space">
                     
                        <div class="col">
                        <div class="input-group">
                                    <label class="label">Feature Description 3</label>
                                    <textarea rows = "2" cols = "100%" name = "feature_description3"  class="input--style-4"><?php echo $feature_description3;?></textarea>
                                </div>
                            </div>
                              <div class="row row-space">

                            <div class="col-2">
                                <div class="input-group">
                                    <label class="label">Feature Icon 4</label>
                                    <img src="../uploadedimages/<?php echo $feature_icon4;?>" width="50px" height="50px">
                                    <input class="input--style-4" type="file" name="feature_icon4" size="50%">
                                </div>
                            </div>
                            <div class="col-2">
                                <div class="input-group">
                                    <label class="label">Feature Name 4</label>
                                    <input class="input--style-4" type="text" name="feature_text4" size="50%" value="<?php echo $feature_text4;?>">
                                </div>
                            </div>
                        </div>
                        <div class="row row-space">
                     
                        <div class="col">
                        <div class="input-group">
                                    <label class="label">Feature Description 4</label>
                                    <textarea rows = "2" cols = "100%" name = "feature_description4"  class="input--style-4"><?php echo $feature_description4;?></textarea>
                                </div>
                            </div>
                              <div class="row row-space">

                            <div class="col-2">
                                <div class="input-group">
                                    <label class="label">Feature Icon 5</label>
                                    <img src="../uploadedimages/<?php echo $feature_icon5;?>" width="50px" height="50px">
                                    <input class="input--style-4" type="file" name="feature_icon5" size="50%">
                                </div>
                            </div>
                            <div class="col-2">
                                <div class="input-group">
                                    <label class="label">Feature Name 5</label>
                                    <input class="input--style-4" type="text" name="feature_text5" size="50%" value="<?php echo $feature_text5;?>">
                                </div>
                            </div>
                        </div>
                        <div class="row row-space">
                     
                        <div class="col-3">
                        <div class="input-group">
                                    <label class="label">Feature Description 5</label>
                                    <textarea rows = "2" cols = "100%" name = "feature_description5"  class="input--style-4"><?php echo $feature_description5;?></textarea>
                                </div>
                            </div>
                        </div>
                            <div class="row row-space">

                            <div class="col-2">
                                <div class="input-group">
                                    <label class="label">Feature Icon 6</label>
                                    <img src="../uploadedimages/<?php echo $feature_icon6;?>" width="50px" height="50px">
                                    <input class="input--style-4" type="file" name="feature_icon6" width="50%">
                                </div>
                            </div>
                            <div class="col-2">
                                <div class="input-group">
                                    <label class="label">Feature Name 6</label>
                                    <input class="input--style-4" type="text" name="feature_text6" size="50%" value="<?php echo $feature_text6;?>">
                                </div>
                            </div>
                        </div>
                        <div class="row row-space">
                     
                        <div class="col">
                        <div class="input-group">
                                    <label class="label">Feature Description 6</label>
                                    <textarea rows = "2" cols = "100%" name = "feature_description6"  class="input--style-4"><?php echo $feature_description6;?></textarea>
                                </div>
                            </div>
                            
                            <?php
                            if($theme=="1")
                            {

                                ?>


                            <h1 class="title border">Testimonials</h1>
                
                             <div class="row row-space">
                            <div class="col-2">
                                <div class="input-group">
                                    <label class="label">Testimonial Icon 1</label>
                                    <img src="../uploadedimages/<?php echo $testimonial_icon1;?>" width="50px" height="50px">
                                    <input class="input--style-4" size="50%" type="file" name="testimonial_icon1">
                                </div>
                            </div>
                             <div class="col-2">
                                <div class="input-group">
                                    <label class="label">Testimonial Name 1</label>
                                    <input class="input--style-4" type="text" name="testimonial_name1" size="50%"value="<?php echo $testimonial_name1;?>">
                                </div>
                            </div>
                        </div>
                        <div class="row row-space"
                            
                            <div class="col">
                                <div class="input-group">
                                    <label class="label">Testimonial1</label>
                                    <input class="input--style-4" type="text" name="testimonial1" size="100" value="<?php echo $testimonial1;?>">
                                </div>
                            </div>
                             
                       
                               </div>
                           
                            <div class="row row-space">
                            <div class="col-2">
                                <div class="input-group">
                                    <label class="label">Testimonial Icon 2</label>
                                    <img src="../uploadedimages/<?php echo $testimonial_icon2;?>" width="50px" height="50px">
                                    <input class="input--style-4" type="file" name="testimonial_icon2" size="50" >
                                </div>
                            </div>
                            <div class="col-2">
                                <div class="input-group">
                                    <label class="label">Testimonial Name 2</label>
                                    <input class="input--style-4" type="text" name="testimonial_name2" size="50%"value="<?php echo $testimonial_name2;?>">
                                </div>
                            </div>
                        </div>
                        <div class="row row-space">
                            <div class="col">
                                <div class="input-group">
                                    <label class="label">Testimonial2</label>
                                    <input class="input--style-4" type="text" name="testimonial2"size="100%" value="<?php echo $testimonial2;?>">
                                </div>
                            </div>
                             
                        
                               </div>
                                <div class="row row-space">
                            <div class="col-2">
                                <div class="input-group">
                                    <label class="label">Testimonial Icon 3</label>
                                    <img src="../uploadedimages/<?php echo $testimonial_icon3;?>" width="50px" height="50px">
                                    <input class="input--style-4" size="50%"type="file" name="testimonial_icon3">
                                </div>
                            </div>
                            <div class="col-2">
                                <div class="input-group">
                                    <label class="label">Testimonial Name 3</label>
                                    <input class="input--style-4" type="text" name="testimonial_name3" size="50%" value="<?php echo $testimonial_name3;?>">
                                </div>
                            </div>
                        </div>
                        <div class="row row-space">
                            
                            <div class="col">
                                <div class="input-group">
                                    <label class="label">Testimonial3</label>
                                    <input class="input--style-4" type="text" name="testimonial3" size="100%" value="<?php echo $testimonial3;?>">
                                </div>
                            </div>
                             
                        
                               </div>

                                <div class="row row-space">
                            <div class="col-2">
                                <div class="input-group">
                                    <label class="label">Testimonial Icon 4</label>
                                    <img src="../uploadedimages/<?php echo $testimonial_icon4;?>" width="50px" height="50px">
                                    <input class="input--style-4" type="file" name="testimonial_icon4" size="50%">
                                </div>
                            </div>
                            <div class="col-2">
                                <div class="input-group">
                                    <label class="label">Testimonial Name 4</label>
                                    <input class="input--style-4" type="text" name="testimonial_name4" value="<?php echo $testimonial_name4;?>" size="50%">
                                </div>
                            </div>
                        </div>
                        <div class="row row-space">
                            
                            <div class="col">
                                <div class="input-group">
                                    <label class="label">Testimonial4</label>
                                    <input class="input--style-4" type="text" name="testimonial4" value="<?php echo $testimonial4;?>" size="100%">
                                </div>
                            </div>
                             
                        
                               </div>
                               <?php
                                }
                               ?>



                                    <?php

                                if($theme=="2")
                                {

                                    ?>

                            <h1 class="title border">Team</h1>
                
                             <div class="row row-space">
                            <div class="col-2">
                                <div class="input-group">
                                    <label class="label">Team Image 1</label>
                                    <img src="../uploadedimages/<?php echo $team_image1;?>" width="50px" height="50px">
                                    <input class="input--style-4" size="50%" type="file" name="team_image1">
                                </div>
                            </div>
                             <div class="col-2">
                                <div class="input-group">
                                    <label class="label">Team Name 1</label>
                                    <input class="input--style-4" type="text" name="team_name1" size="50%" value="<?php echo $team_name1; ?>">
                                </div>
                            </div>
                        </div>
                        <div class="row row-space"
                            
                            <div class="col">
                                <div class="input-group">
                                    <label class="label">Team role1</label>
                                    <input class="input--style-4" type="text" name="team_role1" size="100" value = "<?php echo $team_role1; ?>">
                                </div>
                            </div>
                             
                       
                               </div>
                           
                            <div class="row row-space">
                            <div class="col-2">
                                <div class="input-group">
                                    <label class="label">Team Image 2</label>
                                    <img src="../uploadedimages/<?php echo $team_image2;?>" width="50px" height="50px">
                                    <input class="input--style-4" type="file" name="team_image2" size="50">
                                </div>
                            </div>
                            <div class="col-2">
                                <div class="input-group">
                                    <label class="label">Team Name 2</label>
                                    <input class="input--style-4" type="text" name="team_name2" size="50%" value="<?php echo $team_name2; ?>">
                                </div>
                            </div>
                        </div>
                        <div class="row row-space">
                            <div class="col">
                                <div class="input-group">
                                    <label class="label">Team role2</label>
                                    <input class="input--style-4" type="text" name="team_role2"size="100%" value="<?php echo $team_role2; ?>">
                                </div>
                            </div>
                             
                        
                               </div>
                                <div class="row row-space">
                            <div class="col-2">
                                <div class="input-group">
                                    <label class="label">Team Image 3</label>
                                    <img src="../uploadedimages/<?php echo $team_image3;?>" width="50px" height="50px">
                                    <input class="input--style-4" size="50%"type="file" name="team_image3">
                                </div>
                            </div>
                            <div class="col-2">
                                <div class="input-group">
                                    <label class="label">Team Name 3</label>
                                    
                                    <input class="input--style-4" type="text" name="team_name3" size="50%" value="<?php echo $team_name3; ?>">
                                </div>
                            </div>
                        </div>
                        <div class="row row-space">
                            
                            <div class="col">
                                <div class="input-group">
                                    <label class="label">Team role 3</label>
                                    <input class="input--style-4" type="text" name="team_role3" size="100%" value="<?php echo $team_role3; ?>">
                                </div>
                            </div>
                             
                        
                               </div>

                                
                               <?php
                           }
                           ?>


                                <h1 class="title border">Counter Section</h1>
                    
                        <div class="row row-space">
                            <div class="col-2">
                                <div class="input-group">
                                    <label class="label">Happy Clients</label>
                                    <input class="input--style-4" type="number" name="happy_clients" size="50%" value="<?php echo $happy_clients;?>">
                                </div>
                            </div>
                            <div class="col-2">
                                <div class="input-group">
                                    <label class="label">People's Love</label>
                                    <input class="input--style-4" type="text" name="peoples_love" size="50%" value="<?php echo $peoples_love;?>">
                                </div>
                            </div>
                           
                        </div>


                                <h1 class="title border">Gallery Section</h1>
                    
                        <div class="row row-space">
                            <div class="col-1">
                                <div class="input-group">
                                    <label class="label">Gallery Description</label>
                                    <textarea rows="3" cols="100%"  name="gallery_description" class="input--style-4"value=""><?php echo $gallery_description;?></textarea>
                                </div>
                            </div>
                            

                           
                        </div>
                           <div class="row row-space">
                            <div class="col-3">
                                <div class="input-group">
                                    <label class="label">Gallery Image 1</label>
                                    <img src="../uploadedimages/<?php echo $gallery_img1;?>" width="50px" height="50px">
                                    <input class="input--style-4" type="file" name="gallery_img1">
                                </div>
                            </div>
                            <div class="col-3">
                                <div class="input-group">
                                    <label class="label">Gallery Image 2</label>
                                    <img src="../uploadedimages/<?php echo $gallery_img2;?>" width="50px" height="50px">
                                    <input class="input--style-4" type="file" name="gallery_img2">
                                </div>
                            </div>
                            <div class="col-3">
                                <div class="input-group">
                                    <label class="label">Gallery Image 3</label>
                                    <img src="../uploadedimages/<?php echo $gallery_img3;?>" width="50px" height="50px">
                                    <input class="input--style-4" type="file" name="gallery_img3">
                                </div>
                            </div>

                        </div>
                          <div class="row row-space">
                            <div class="col-3">
                                <div class="input-group">
                                    <label class="label">Gallery Image 4</label>
                                    <img src="../uploadedimages/<?php echo $gallery_img4;?>" width="50px" height="50px">
                                    <input class="input--style-4" type="file" name="gallery_img4">
                                </div>
                            </div>
                            <div class="col-3">
                                <div class="input-group">
                                    <label class="label">Gallery Image 5</label>
                                    <img src="../uploadedimages/<?php echo $gallery_img5;?>" width="50px" height="50px">
                                    <input class="input--style-4" type="file" name="gallery_img5">
                                </div>
                            </div>
                            <div class="col-3">
                                <div class="input-group">
                                    <label class="label">Gallery Image 6</label>
                                    <img src="../uploadedimages/<?php echo $gallery_img6;?>" width="50px" height="50px">
                                    <input class="input--style-4" type="file" name="gallery_img6">
                                </div>
                            </div>

                        </div>
                        <?php 
                        if($theme=="2")
                        {

                            ?>

                        <div class="row row-space">
                            <div class="col-2">
                                <div class="input-group">
                                    <label class="label">Gallery Image 7</label>
                                    <img src="../uploadedimages/<?php echo $gallery_img7;?>" width="50px" height="50px">
                                    <input class="input--style-4" type="file" name="gallery_img7">
                                </div>
                            </div>
                            <div class="col-2">
                                <div class="input-group">
                                    <label class="label">Gallery Image 8</label>
                                    <img src="../uploadedimages/<?php echo $gallery_img8;?>" width="50px" height="50px">
                                    <input class="input--style-4" type="file" name="gallery_img8">
                                </div>
                            </div>
                            

                        </div>
                        <?php
                    }
                    ?>

                        <h1 class="title border">Contact</h1>
                    
                        <div class="row row-space">
                            <div class="col-3">
                                <div class="input-group">
                                    <label class="label">Address</label>
                                    <input class="input--style-4" type="text" name="address" value="<?php echo $address;?>">
                                </div>
                            </div>
                            <div class="col-3">
                                <div class="input-group">
                                    <label class="label">Place</label>
                                    <input class="input--style-4" type="text" name="place" value="<?php echo $place;?>">
                                </div>
                            </div>
                            <div class="col-3">
                                <div class="input-group">
                                    <label class="label">Zip Code</label>
                                    <input class="input--style-4" type="number" name="zip" value="<?php echo $zip;?>">
                                </div>
                            </div>
                           
                        </div>
                        <div class="row row-space">
                            <div class="col-3">
                                <div class="input-group">
                                    <label class="label">Opening Hours</label>
                                    <input class="input--style-4" type="text" name="hours" value="<?php echo $hours;?>">
                                </div>
                            </div>
                            <div class="col-3">
                                <div class="input-group">
                                    <label class="label">Phone Number</label>
                                    <input class="input--style-4" type="number" name="phone" value="<?php echo $phone;?>">
                                </div>
                            </div>
                            <div class="col-3">
                                <div class="input-group">
                                    <label class="label">Email</label>
                                    <input class="input--style-4" type="email" name="email" value="<?php echo $email;?>">
                                </div>
                            </div>
                           
                        </div>


                        <div class="p-t-15">
                            <input type="hidden" name="id" value="<?php echo $id;?>">
                            <input type="hidden" name="logo" value="<?php echo $logo;?>">
                            <input type="hidden" name="service_icon1" value="<?php echo $service_icon1;?>">
                            <input type="hidden" name="service_icon2" value="<?php echo $service_icon2;?>">
                            <input type="hidden" name="service_icon3" value="<?php echo $service_icon3;?>">
                            <input type="hidden" name="feature_icon1" value="<?php echo $feature_icon1;?>">
                            <input type="hidden" name="feature_icon2" value="<?php echo $feature_icon2;?>">
                            <input type="hidden" name="feature_icon3" value="<?php echo $feature_icon3;?>">
                            <input type="hidden" name="feature_icon4" value="<?php echo $feature_icon4;?>">
                            <input type="hidden" name="feature_icon5" value="<?php echo $feature_icon5;?>">
                            <input type="hidden" name="feature_icon6" value="<?php echo $feature_icon6;?>">
                            <input type="hidden" name="testimonial_icon1" value="<?php echo $testimonial_icon1;?>">
                            <input type="hidden" name="testimonial_icon2" value="<?php echo $testimonial_icon2;?>">
                            <input type="hidden" name="testimonial_icon3" value="<?php echo $testimonial_icon3;?>">
                            <input type="hidden" name="testimonial_icon4" value="<?php echo $testimonial_icon4;?>">
                            <input type="hidden" name="team_image1" value="<?php echo $team_image1;?>">
                             <input type="hidden" name="team_image2" value="<?php echo $team_image2;?>"> <input type="hidden" name="team_image3" value="<?php echo $team_image3;?>">
                            
                            <input type="hidden" name="gallery_img1" value="<?php echo $gallery_img1;?>">
                            <input type="hidden" name="gallery_img2" value="<?php echo $gallery_img2;?>">
                            <input type="hidden" name="gallery_img3" value="<?php echo $gallery_img3;?>">
                            <input type="hidden" name="gallery_img4" value="<?php echo $gallery_img4;?>">
                            <input type="hidden" name="gallery_img5" value="<?php echo $gallery_img5;?>">
                            <input type="hidden" name="gallery_img6" value="<?php echo $gallery_img6;?>">
                            <input type="hidden" name="gallery_img7" value="<?php echo $gallery_img7;?>">
                            <input type="hidden" name="gallery_img8" value="<?php echo $gallery_img8;?>">
                            <input type="hidden" name="theme" value = "<?php echo $theme_name;?>">
                            <input class="btn btn--radius-2 btn--blue" type="submit" id="submit" name="submit" value="update">
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <!-- Jquery JS-->
    <script src="vendor1/jquery/jquery.min.js"></script>
    <!-- Vendor JS-->
    <script src="vendor1/select2/select2.min.js"></script>
    <script src="vendor1/datepicker/moment.min.js"></script>
    <script src="vendor1/datepicker/daterangepicker.js"></script>

    <!-- Main JS-->
    <script src="js1/global.js"></script>
    <script src="js1/validate.js"></script>
</body><!-- This templates was made by Colorlib (https://colorlib.com) -->

</html>
<!-- end document-->