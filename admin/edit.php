<!DOCTYPE html>
<html>
<head>
	<title>Edit</title>
</head>
<body>

	<form method = "post" action = "connect.php" enctype="multipart/form-data">
		<fieldset>
			<legend>Basic Info:</legend>
				<label for="sitename">Site Name:</label>
				<input type="text" name="sitename"><br>

				<label for="logo">Logo :</label>
				<input type="file" name="image"><br>

				<label for="url">URL:</label>
				<input type="text" name="url"><br>

				<label for="title">Title:</label>
				<input type="text" name="title"><br>

		</fieldset>
		<fieldset>
			<legend>Banner info</legend>
				<label for ="heading">Heading :</label>
				<input type="text" name="heading"><br>

				<label for ="caption">Caption :</label>
				<input type="text" name="caption"><br>

		</fieldset>
		<fieldset>
			<legend>About</legend>

				<label for ="about">About :</label>
				<input type="text" name="about"><br>
		</fieldset>

		<fieldset>
			<legend>Serivces info</legend>
				<label for ="heading">service Intro :</label>
				<input type="text" name="services"><br>

				
				<label for="service_icon1">Service Icon 1 :</label>
				<input type="file" name="service_icon1"><br>

				<label for ="service_text1">Service Name 1 :</label>
				<input type="text" name="service_text1"><br>

				<label for ="service_description1">Service Description 1 :</label>
				<input type="text" name="service_description1"><br>

				<label for="service_icon2">Service Icon 12:</label>
				<input type="file" name="service_icon2"><br>

				<label for ="service_text2">Service Name 2 :</label>
				<input type="text" name="service_text2"><br>

				<label for ="service_description2">Service Description 2 :</label>
				<input type="text" name="service_description2"><br>


		</fieldset>
		<fieldset>
			<legend>Features info</legend>
				<label for ="features_intro">Features Intro :</label>
				<input type="text" name="features"><br>

				
				<label for="feature_icon1">Feature Icon 1 :</label>
				<input type="file" name="feature_icon1"><br>

				<label for ="feature_text1">Feature Name 1 :</label>
				<input type="text" name="feature_text1"><br>

				<label for ="feature_description1">Feature Description 1 :</label>
				<input type="text" name="feature_description1"><br>

				<label for="feature_icon2">Feature Icon 2:</label>
				<input type="file" name="feature_icon2"><br>

				 <label for ="feature_text2">Feature Name 2 :</label>
				<input type="text" name="feature_text2"><br>

				<label for ="feature_description2">Feature Description 2 :</label>
				<input type="text" name="feature_description2"><br>

				<label for="feature_icon3">Feature Icon 3:</label>
				<input type="file" name="feature_icon3"><br>

				<label for ="feature_text3">Feature Name 3 :</label>
				<input type="text" name="feature_text3"><br>

				<label for ="feature_description3">Feature Description 3 :</label>
				<input type="text" name="feature_description3"><br>

				<label for="feature_icon4">Feature Icon 4:</label>
				<input type="file" name="feature_icon4"><br>

				<label for ="feature_text4">Feature Name 4 :</label>
				<input type="text" name="feature_text4"><br>

				<label for ="feature_description4">Feature Description 4 :</label>
				<input type="text" name="feature_description4"><br>

				<label for="feature_icon5">Feature Icon 5:</label>
				<input type="file" name="feature_icon5"><br>

				<label for ="feature_text5">Feature Name 5 :</label>
				<input type="text" name="feature_text5"><br>

				<label for ="feature_description5">Feature Description 5 :</label>
				<input type="text" name="feature_description5"><br>

				<label for="feature_icon6">Feature Icon 6:</label>
				<input type="file" name="feature_icon6"><br>

				<label for ="feature_text6">Feature Name 6 :</label>
				<input type="text" name="feature_text6"><br>

				<label for ="feature_description6">Feature Description 6 :</label>
				<input type="text" name="feature_description6"><br>


		</fieldset>

		<fieldset>

				<legend>Testimonials</legend>


				<label for="testimonial_icon1">Testimonial Icon 1 :</label>
				<input type="file" name="testimonial_icon1"><br>

				<label for ="testimonial1">Testimonial 1 :</label>
				<input type="text" name="testimonial1"><br>

				<label for ="testimonial_name1">Testimonial Name 1 :</label>
				<input type="text" name="testimonial_name1"><br>

				<label for="testimonial_icon2">Testimonial Icon 2 :</label>
				<input type="file" name="testimonial_icon2"><br>

				<label for ="testimonial2">Testimonial 2 :</label>
				<input type="text" name="testimonial2"><br>

				<label for ="testimonial_name2">Testimonial Name 2 :</label>
				<input type="text" name="testimonial_name2"><br>

				<label for="testimonial_icon3">Testimonial Icon 3 :</label>
				<input type="file" name="testimonial_icon3"><br>

				<label for ="testimonial3">Testimonial 3 :</label>
				<input type="text" name="testimonial3"><br>

				<label for ="testimonial_name3">Testimonial Name 3 :</label>
				<input type="text" name="testimonial_name3"><br>

				<label for="testimonial_icon4">Testimonial Icon 4 :</label>
				<input type="file" name="testimonial_icon4"><br>

				<label for ="testimonial4">Testimonial 4 :</label>
				<input type="text" name="testimonial4"><br>

				<label for ="testimonial_name4">Testimonial Name 4 :</label>
				<input type="text" name="testimonial_name4"><br>
		</fieldset>
		<fieldset>
			
			<legend>Counter Section</legend>
				<label for ="happly_clients">Happy Clients :</label>
				<input type="number" name="happy_clients"><br>

				<label for ="peoples_love">Peoples Love :</label>
				<input type="number" name="peoples_love"><br>

		</fieldset>

		<fieldset>
			
			<legend>Gallery Info</legend>
				<label for ="gallery_description">Gallery Description :</label>
				<input type="text" name="gallery_description"><br>

				 <label for="gallery_img1">Gallery Image1 :</label>
				<input type="file" name="gallery_img1"><br>

				<label for="gallery_img2">Gallery Image2 :</label>
				<input type="file" name="gallery_img2"><br>

				<label for="gallery_img3">Gallery Image3 :</label>
				<input type="file" name="gallery_img3"><br>

				<label for="gallery_img4">Gallery Image4 :</label>
				<input type="file" name="gallery_img4"><br>

				<label for="gallery_img5">Gallery Image5 :</label>
				<input type="file" name="gallery_img5"><br>

				<label for="gallery_img6">Gallery Image6 :</label>
				<input type="file" name="gallery_img6"><br>

		</fieldset>
			<input type="submit" name="submit" value="Update">




	</form>

</body>
</html>