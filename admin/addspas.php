<?php
session_start();
if($_SESSION['type']=='Admin')
{
    if(isset($_GET["id"]))
    {

        $themeid = $_GET["id"];
        
    }
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Required meta tags-->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Colorlib Templates">
    <meta name="author" content="Colorlib">
    <meta name="keywords" content="Colorlib Templates">

    <!-- Title Page-->
    <title>Form</title>

    <!-- Icons font CSS-->
    <link href="../vendor1/mdi-font/css/material-design-iconic-font.min.css" rel="stylesheet" media="all">
    <link href="../vendor1/font-awesome-4.7/css/font-awesome.min.css" rel="stylesheet" media="all">
    <!-- Font special for pages-->
    <link href="https://fonts.googleapis.com/css?family=Poppins:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

    <!-- Vendor CSS-->
    <link href="../vendor1/select2/select2.min.css" rel="stylesheet" media="all">
    <link href="../vendor1/datepicker/daterangepicker.css" rel="stylesheet" media="all">

    <!-- Main CSS-->
<!--     <script type="text/javascript">
        function showDiv(divId, element)
{

    document.getElementById(divId).style.display = element.value == 2 ? 'block' : 'none';
}


    </script> -->
        <link href="../css1/main.css" rel="stylesheet" media="all">

</head>

<body>
    <div class="page-wrapper bg-gra-02 p-t-130 p-b-100 font-poppins">
        <div class="wrapper wrapper--w680">
            <div class="card card-4">
                <div class="card-body">
                    <h1 class="title">Basic Information</h1>
                   <form name="f1" method="post"  action = "connect.php" enctype="multipart/form-data">
                        <div id="messages"></div>
                     
                       
                        <div class="row row-space">
                            <div class="col-2">
                                <div class="input-group ">
                                    <label class="label required">Site Name</label>
                                    <input class="input--style-4" type="text" name="sitename" >
                                </div>
                            </div>
                            <div class="col-2">
                                <div class="input-group">
                                    <label class="label">URL</label>
                                    <input class="input--style-4" type="text" name="url">
                                </div>
                            </div>
                        </div>
                        <div class = "row row-space">
                            <div class="col">
                                <div class="input-group ">
                                    <label class="label required">Title</label>
                                    <input class="input--style-4" type="text" name="title" id="title" size=100 style= 
                                    "width:100%">
                                </div>
                            </div>
                        </div>
                        <div class = "row row-space">
                            <div class="col-2">
                                <div class="input-group ">
                                    <label class="label required">Logo</label>
                                    <input class="input--style-4" type="file" name="image" id="image">
                                </div>
                            </div>
                            <div class="col-2">
                                <div class="input-group ">
                                    <label class="label required">Logo Text</label>
                                    <input class="input--style-4" type="text" name="logo_text" id="logo_text">
                                </div>
                            </div>
                        </div>
                       
                       
                        
                        
                    
                    <h1 class="title border">Banner Information</h1>
                    
                        <div class="row row-space">
                            <div class="col">
                                <div class="input-group">
                                    <label class="label">Heading</label>
                                    <input class="input--style-4" type="text" name="heading" size="100%">
                                </div>
                            </div>
                        </div>
                        <div class="row row-space">
                            <div class="col">
                                <div class="input-group">
                                    <label class="label">Caption</label>
                                    <input class="input--style-4" type="text" name="caption" size="100%">
                                </div>
                            </div>
                           
                        </div>
                         <h1 class="title border">About Information</h1>
                    
                        <div class="row row-space">
                           <!--  <div class="col">
                                <div class="input-group">
                                    <label class="label">About</label>
                                    <textarea rows="5" cols="20" name="about" ><?php  $about;?></textarea>
                                </div>
                            </div>
                            -->
                            <div class="col">
                                <div class="input-group">
                                    <label class="label">About</label>
                                    <textarea rows = "5" cols="100%" name = "about"  class="input--style-4"></textarea>
                                </div>
                            </div>
                           
                        </div>
                      
                       
                    <h1 class="title border">Service Information</h1>
                    
                        <div class="row row-space">
                            <div class="col">
                                <div class="input-group">
                                    <label class="label">Service Intro</label>
                                    <textarea rows = "3" cols="100%" name = "services"  class="input--style-4"></textarea>
                                </div>
                            </div>
                            
                        </div>
                             <div class="row row-space">

                            <div class="col-2">
                                <div class="input-group">
                                    <label class="label">Service Icon 1</label>
                                    <input class="input--style-4" type="file" name="service_icon1">
                                </div>
                            </div>
                            <div class="col-2">
                                <div class="input-group">
                                    <label class="label">Service Name 1</label>
                                    <input class="input--style-4" type="text" name="service_text1">
                                </div>
                            </div>
                        </div>
                           <div class="row row-space">
                     
                        <div class="col">
                                <div class="input-group">
                                    <label class="label">Service Description 1</label>
                                    <textarea rows = "2" cols = "100%" name = "service_description1"  class="input--style-4"></textarea>
                                </div>
                            </div>
                               </div>
                           <div class="row row-space">

                            <div class="col-2">
                                <div class="input-group">
                                    <label class="label">Service Icon 2</label>
                                    <input class="input--style-4" type="file" name="service_icon2">
                                </div>
                            </div>
                            <div class="col-2">
                                <div class="input-group">
                                    <label class="label">Service Name 2</label>
                                    <input class="input--style-4" type="text" name="service_text2">
                                </div>
                            </div>
                     </div>
                     <div class="row row-space">
                        <div class="col-3">
                                <div class="input-group">
                                    <label class="label">Service Description 2</label>
                                    <textarea rows = "2" cols = "100%" name = "service_description2" class="input--style-4" ></textarea>
                                </div>
                            </div>
                               </div>

                               <?php
                               if($themeid=="2")
                               {
                                ?>
                               <div id="hidden_div">


                            <div class="row row-space">

                            <div class="col-2">
                                <div class="input-group">
                                    <label class="label">Service Icon 3</label>
                                    <input class="input--style-4" type="file" name="service_icon3">
                                </div>
                            </div>
                            <div class="col-2">
                                <div class="input-group">
                                    <label class="label">Service Name 3</label>
                                    <input class="input--style-4" type="text" name="service_text3">
                                </div>
                            </div>
                     </div>
                     <div class="row row-space">
                        <div class="col-3">
                                <div class="input-group">
                                    <label class="label">Service Description 3</label>
                                    <textarea rows = "2" cols = "100%" name = "service_description3" class="input--style-4" ></textarea>
                                </div>
                            </div>
                               </div>

                            </div>
                            <?php
                            }
                            ?>


                               <h1 class="title border">Feature Information</h1>
                    
                        <div class="row row-space">
                            <div class="col">
                                <div class="input-group">
                                    <label class="label">Feature Intro</label>
                                    <textarea rows = "3" cols="100%" name = "features"  class="input--style-4"></textarea>
                                </div>
                            </div>
                            
                        </div>
                             <div class="row row-space">

                            <div class="col-2">
                                <div class="input-group">
                                    <label class="label">Feature Icon 1</label>
                                    <input class="input--style-4" type="file" name="feature_icon1">
                                </div>
                            </div>
                            <div class="col-2">
                                <div class="input-group">
                                    <label class="label">Feature Name 1</label>
                                    <input class="input--style-4" type="text" name="feature_text1">
                                </div>
                            </div>
                        </div>
                        <div class="row row-space">
                     
                        <div class="col">
                        <div class="input-group">
                                    <label class="label">Feature Description 1</label>
                                    <div class="input-group">
                                    <label class="label"></label>
                                    <textarea rows="2" cols="100%" name="feature_description1" class="input--style-4"  ></textarea>
                                </div>
                                </div>
                            </div>
                               </div>
                           <div class="row row-space">

                            <div class="col-2">
                                <div class="input-group">
                                    <label class="label">Feature Icon 2</label>
                                    <input class="input--style-4" type="file" name="feature_icon2">
                                </div>
                            </div>
                            <div class="col-2">
                                <div class="input-group">
                                    <label class="label">Feature Name 2</label>
                                    <input class="input--style-4" type="text" name="feature_text2">
                                </div>
                            </div>
                     </div>
                     <div class="row row-space">
                        <div class="col">
                        <div class="input-group">
                                    <label class="label">Feature Description 2</label>
                                    <textarea rows = "2" cols = "100%" name = "feature_description2"  class="input--style-4"></textarea>
                                </div>
                            </div>
                            <div class="row row-space">

                            <div class="col-2">
                                <div class="input-group">
                                    <label class="label">Feature Icon 3</label>
                                    <input class="input--style-4" type="file" name="feature_icon3" size="50%">
                                </div>
                            </div>
                            <div class="col-2">
                                <div class="input-group">
                                    <label class="label">Feature Name 3</label>
                                    <input class="input--style-4" type="text" name="feature_text3" size="50%">
                                </div>
                            </div>
                        </div>
                        <div class="row row-space">
                     
                        <div class="col">
                        <div class="input-group">
                                    <label class="label">Feature Description 3</label>
                                    <textarea rows = "2" cols = "100%" name = "feature_description3"  class="input--style-4"></textarea>
                                </div>
                            </div>
                              <div class="row row-space">

                            <div class="col-2">
                                <div class="input-group">
                                    <label class="label">Feature Icon 4</label>
                                    <input class="input--style-4" type="file" name="feature_icon4" size="50%">
                                </div>
                            </div>
                            <div class="col-2">
                                <div class="input-group">
                                    <label class="label">Feature Name 4</label>
                                    <input class="input--style-4" type="text" name="feature_text4" size="50%">
                                </div>
                            </div>
                        </div>
                        <div class="row row-space">
                     
                        <div class="col">
                        <div class="input-group">
                                    <label class="label">Feature Description 4</label>
                                    <textarea rows = "2" cols = "100%" name = "feature_description4"  class="input--style-4"></textarea>
                                </div>
                            </div>
                              <div class="row row-space">

                            <div class="col-2">
                                <div class="input-group">
                                    <label class="label">Feature Icon 5</label>
                                    <input class="input--style-4" type="file" name="feature_icon5" size="50%">
                                </div>
                            </div>
                            <div class="col-2">
                                <div class="input-group">
                                    <label class="label">Feature Name 5</label>
                                    <input class="input--style-4" type="text" name="feature_text5" size="50%">
                                </div>
                            </div>
                        </div>
                        <div class="row row-space">
                     
                        <div class="col-3">
                        <div class="input-group">
                                    <label class="label">Feature Description 5</label>
                                    <textarea rows = "2" cols = "100%" name = "feature_description5"  class="input--style-4"></textarea>
                                </div>
                            </div>
                        </div>
                            <div class="row row-space">

                            <div class="col-2">
                                <div class="input-group">
                                    <label class="label">Feature Icon 6</label>
                                    <input class="input--style-4" type="file" name="feature_icon6" width="50%">
                                </div>
                            </div>
                            <div class="col-2">
                                <div class="input-group">
                                    <label class="label">Feature Name 6</label>
                                    <input class="input--style-4" type="text" name="feature_text6" size="50%">
                                </div>
                            </div>
                        </div>
                        <div class="row row-space">
                     
                        <div class="col">
                        <div class="input-group">
                                    <label class="label">Feature Description 6</label>
                                    <textarea rows = "2" cols = "100%" name = "feature_description6"  class="input--style-4"></textarea>
                                </div>
                            </div>
                              
                            <?php

                                if($themeid==1)
                                {

                                    ?>

                            <h1 class="title border">Testimonials</h1>
                
                             <div class="row row-space">
                            <div class="col-2">
                                <div class="input-group">
                                    <label class="label">Testimonial Icon 1</label>
                                    <input class="input--style-4" size="50%" type="file" name="testimonial_icon1">
                                </div>
                            </div>
                             <div class="col-2">
                                <div class="input-group">
                                    <label class="label">Testimonial Name 1</label>
                                    <input class="input--style-4" type="text" name="testimonial_name1" size="50%">
                                </div>
                            </div>
                        </div>
                        <div class="row row-space"
                            
                            <div class="col">
                                <div class="input-group">
                                    <label class="label">Testimonial1</label>
                                    <input class="input--style-4" type="text" name="testimonial1" size="100">
                                </div>
                            </div>
                             
                       
                               </div>
                           
                            <div class="row row-space">
                            <div class="col-2">
                                <div class="input-group">
                                    <label class="label">Testimonial Icon 2</label>
                                    <input class="input--style-4" type="file" name="testimonial_icon2" size="50">
                                </div>
                            </div>
                            <div class="col-2">
                                <div class="input-group">
                                    <label class="label">Testimonial Name 2</label>
                                    <input class="input--style-4" type="text" name="testimonial_name2" size="50%">
                                </div>
                            </div>
                        </div>
                        <div class="row row-space">
                            <div class="col">
                                <div class="input-group">
                                    <label class="label">Testimonial2</label>
                                    <input class="input--style-4" type="text" name="testimonial2"size="100%">
                                </div>
                            </div>
                             
                        
                               </div>
                                <div class="row row-space">
                            <div class="col-2">
                                <div class="input-group">
                                    <label class="label">Testimonial Icon 3</label>
                                    <input class="input--style-4" size="50%"type="file" name="testimonial_icon3">
                                </div>
                            </div>
                            <div class="col-2">
                                <div class="input-group">
                                    <label class="label">Testimonial Name 3</label>
                                    <input class="input--style-4" type="text" name="testimonial_name3" size="50%">
                                </div>
                            </div>
                        </div>
                        <div class="row row-space">
                            
                            <div class="col">
                                <div class="input-group">
                                    <label class="label">Testimonial3</label>
                                    <input class="input--style-4" type="text" name="testimonial3" size="100%">
                                </div>
                            </div>
                             
                        
                               </div>

                                <div class="row row-space">
                            <div class="col-2">
                                <div class="input-group">
                                    <label class="label">Testimonial Icon 4</label>
                                    <input class="input--style-4" type="file" name="testimonial_icon4" size="50%">
                                </div>
                            </div>
                            <div class="col-2">
                                <div class="input-group">
                                    <label class="label">Testimonial Name 4</label>
                                    <input class="input--style-4" type="text" name="testimonial_name4" size="50%">
                                </div>
                            </div>
                        </div>
                        <div class="row row-space">
                            
                            <div class="col">
                                <div class="input-group">
                                    <label class="label">Testimonial4</label>
                                    <input class="input--style-4" type="text" name="testimonial4"size="100%">
                                </div>
                            </div>
                             
                        
                               </div>
                               <?php
                           }
                           ?>


                            <?php

                                if($themeid==2)
                                {

                                    ?>

                            <h1 class="title border">Team</h1>
                
                             <div class="row row-space">
                            <div class="col-2">
                                <div class="input-group">
                                    <label class="label">Team Image 1</label>
                                    <input class="input--style-4" size="50%" type="file" name="team_image1">
                                </div>
                            </div>
                             <div class="col-2">
                                <div class="input-group">
                                    <label class="label">Team Name 1</label>
                                    <input class="input--style-4" type="text" name="team_name1" size="50%">
                                </div>
                            </div>
                        </div>
                        <div class="row row-space"
                            
                            <div class="col">
                                <div class="input-group">
                                    <label class="label">Team role1</label>
                                    <input class="input--style-4" type="text" name="team_role1" size="100">
                                </div>
                            </div>
                             
                       
                               </div>
                           
                            <div class="row row-space">
                            <div class="col-2">
                                <div class="input-group">
                                    <label class="label">Team Image 2</label>
                                    <input class="input--style-4" type="file" name="team_image2" size="50">
                                </div>
                            </div>
                            <div class="col-2">
                                <div class="input-group">
                                    <label class="label">Team Name 2</label>
                                    <input class="input--style-4" type="text" name="team_name2" size="50%">
                                </div>
                            </div>
                        </div>
                        <div class="row row-space">
                            <div class="col">
                                <div class="input-group">
                                    <label class="label">Team role2</label>
                                    <input class="input--style-4" type="text" name="team_role2"size="100%">
                                </div>
                            </div>
                             
                        
                               </div>
                                <div class="row row-space">
                            <div class="col-2">
                                <div class="input-group">
                                    <label class="label">Team Image 3</label>
                                    <input class="input--style-4" size="50%"type="file" name="team_image3">
                                </div>
                            </div>
                            <div class="col-2">
                                <div class="input-group">
                                    <label class="label">Team Name 3</label>
                                    <input class="input--style-4" type="text" name="team_name3" size="50%">
                                </div>
                            </div>
                        </div>
                        <div class="row row-space">
                            
                            <div class="col">
                                <div class="input-group">
                                    <label class="label">Team role 3</label>
                                    <input class="input--style-4" type="text" name="team_role3" size="100%">
                                </div>
                            </div>
                             
                        
                               </div>

                                
                               <?php
                           }
                           ?>



                                <h1 class="title border">Counter Section</h1>
                    
                        <div class="row row-space">
                            <div class="col-2">
                                <div class="input-group">
                                    <label class="label">Happy Clients</label>
                                    <input class="input--style-4" type="number" name="happy_clients" size="50%">
                                </div>
                            </div>
                            <div class="col-2">
                                <div class="input-group">
                                    <label class="label">People's Love</label>
                                    <input class="input--style-4" type="text" name="peoples_love" size="50%">
                                </div>
                            </div>
                           
                        </div>


                                <h1 class="title border">Gallery Section</h1>
                    
                        <div class="row row-space">
                            <div class="col-1">
                                <div class="input-group">
                                    <label class="label">Gallery Description</label>
                                    <textarea rows="3" cols="100%"  name="gallery_description" class="input--style-4"></textarea>
                                </div>
                            </div>
                            

                           
                        </div>
                           <div class="row row-space">
                            <div class="col-3">
                                <div class="input-group">
                                    <label class="label">Gallery Image 1</label>
                                    <input class="input--style-4" type="file" name="gallery_img1">
                                </div>
                            </div>
                            <div class="col-3">
                                <div class="input-group">
                                    <label class="label">Gallery Image 2</label>
                                    <input class="input--style-4" type="file" name="gallery_img2">
                                </div>
                            </div>
                            <div class="col-3">
                                <div class="input-group">
                                    <label class="label">Gallery Image 3</label>
                                    <input class="input--style-4" type="file" name="gallery_img3">
                                </div>
                            </div>

                        </div>
                          <div class="row row-space">
                            <div class="col-3">
                                <div class="input-group">
                                    <label class="label">Gallery Image 4</label>
                                    <input class="input--style-4" type="file" name="gallery_img4">
                                </div>
                            </div>
                            <div class="col-3">
                                <div class="input-group">
                                    <label class="label">Gallery Image 5</label>
                                    <input class="input--style-4" type="file" name="gallery_img5">
                                </div>
                            </div>
                            <div class="col-3">
                                <div class="input-group">
                                    <label class="label">Gallery Image 6</label>
                                    <input class="input--style-4" type="file" name="gallery_img6">
                                </div>
                            </div>

                        </div>


                        <?php
                        
                            if($themeid==2)

                            {
                                ?>

                    <div id="hidden_div">
                        <div class="row row-space" >
                            <div class="col-2">
                                <div class="input-group">
                                    <label class="label">Gallery Image 7</label>
                                    <input class="input--style-4" type="file" name="gallery_img7">
                                </div>
                            </div>
                            <div class="col-2">
                                <div class="input-group">
                                    <label class="label">Gallery Image 8</label>
                                    <input class="input--style-4" type="file" name="gallery_img8">
                                </div>
                            </div>
                        </div>
                    </div>

                    <?php
                }
                ?>

                        <h1 class="title border">Contact</h1>
                    
                        <div class="row row-space">
                            <div class="col-3">
                                <div class="input-group">
                                    <label class="label">Address</label>
                                    <input class="input--style-4" type="text" name="address">
                                </div>
                            </div>
                            <div class="col-3">
                                <div class="input-group">
                                    <label class="label">Place</label>
                                    <input class="input--style-4" type="text" name="place">
                                </div>
                            </div>
                            <div class="col-3">
                                <div class="input-group">
                                    <label class="label">Zip Code</label>
                                    <input class="input--style-4" type="number" name="zip">
                                </div>
                            </div>
                           
                        </div>
                        <div class="row row-space">
                            <div class="col-3">
                                <div class="input-group">
                                    <label class="label">Opening Hours</label>
                                    <input class="input--style-4" type="text" name="hours">
                                </div>
                            </div>
                            <div class="col-3">
                                <div class="input-group">
                                    <label class="label">Phone Number</label>
                                    <input class="input--style-4" type="number" name="phone" >
                                </div>
                            </div>
                            <div class="col-3">
                                <div class="input-group">
                                    <label class="label">Email</label>
                                    <input class="input--style-4" type="email" name="email">
                                </div>
                            </div>
                           
                        </div>


                        <div class="p-t-15">
                            <input type="hidden" name="themeid" value = "<?php echo $themeid;?>">
                            <input class="btn btn--radius-2 btn--blue" type="submit" id="submit" name="submit">
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <!-- Jquery JS-->
    <script src="../vendor1/jquery/jquery.min.js"></script>
    <!-- Vendor JS-->
    <script src="../vendor1/select2/select2.min.js"></script>
    <script src="../vendor1/datepicker/moment.min.js"></script>
    <script src="../vendor1/datepicker/daterangepicker.js"></script>

    <!-- Main JS-->
    <script src="../js1/global.js"></script>
    <script src="../js1/validate.js"></script>
</body><!-- This templates was made by Colorlib (https://colorlib.com) -->

</html>
<?php
}
else
{
  header("location:index.php");
}
?>
<!-- end document-->