<?php
   include_once('connection.php');
    $id = mysqli_real_escape_string($con,$_POST["id"]);
        $theme = mysqli_real_escape_string($con,$_POST["theme"]);

    $sitename = mysqli_real_escape_string($con,$_POST["sitename"]);
    $url = mysqli_real_escape_string($con,$_POST["url"]);
    $title = mysqli_real_escape_string($con,$_POST["title"]);
    $logo_text = mysqli_real_escape_string($con,$_POST["logo_text"]);
    $heading = mysqli_real_escape_string($con,$_POST["heading"]);
    $caption = mysqli_real_escape_string($con,$_POST["caption"]);
    $services = mysqli_real_escape_string($con,$_POST['services']);
    $about = mysqli_real_escape_string($con,$_POST["about"]);
    $services = mysqli_real_escape_string($con,$_POST["services"]);
    $service_text1 = mysqli_real_escape_string($con,$_POST["service_text1"]);
    $service_description1 = mysqli_real_escape_string($con,$_POST["service_description1"]);
   
    $service_text2 = mysqli_real_escape_string($con,$_POST["service_text2"]);
    $service_description2 = mysqli_real_escape_string($con,$_POST["service_description2"]);
    if($theme=="2")
    {
    $service_text3 = mysqli_real_escape_string($con,$_POST["service_text3"]);
    $service_description3 = mysqli_real_escape_string($con,$_POST["service_description3"]);
    }

    $features = mysqli_real_escape_string($con,$_POST["features"]);
    $feature_text1 = mysqli_real_escape_string($con,$_POST["feature_text1"]);
    $feature_description1 = mysqli_real_escape_string($con,$_POST["feature_description1"]);
   //$feature_description1 = htmlspecialchars("$feature_description1");
    
    $feature_text2 = mysqli_real_escape_string($con,$_POST["feature_text2"]);
    $feature_description2 = mysqli_real_escape_string($con,$_POST["feature_description2"]);
    $feature_text3 = mysqli_real_escape_string($con,$_POST["feature_text3"]);
    $feature_description3 = mysqli_real_escape_string($con,$_POST["feature_description3"]);
    $feature_text4 = mysqli_real_escape_string($con,$_POST["feature_text4"]);
    $feature_description4 = mysqli_real_escape_string($con,$_POST["feature_description4"]);
    $feature_text5 = mysqli_real_escape_string($con,$_POST["feature_text5"]);
    $feature_description5 = mysqli_real_escape_string($con,$_POST["feature_description5"]);
    $feature_text6 = mysqli_real_escape_string($con,$_POST["feature_text6"]);
    $feature_description6 = mysqli_real_escape_string($con,$_POST["feature_description6"]);

    if($theme=="1")
    {

    $testimonial1 = mysqli_real_escape_string($con,$_POST["testimonial1"]);
    $testimonial_name1 = mysqli_real_escape_string($con,$_POST["testimonial_name1"]);

    $testimonial2 = mysqli_real_escape_string($con,$_POST["testimonial2"]);
    $testimonial_name2 = mysqli_real_escape_string($con,$_POST["testimonial_name2"]);

    $testimonial3 = mysqli_real_escape_string($con,$_POST["testimonial3"]);
    $testimonial_name3 = mysqli_real_escape_string($con,$_POST["testimonial_name3"]);

    $testimonial4 = mysqli_real_escape_string($con,$_POST["testimonial4"]);
    $testimonial_name4 = mysqli_real_escape_string($con,$_POST["testimonial_name4"]);
  }


  if($theme=="2")
{
  $team_name1 = mysqli_real_escape_string($con,$_POST["team_name1"]);
    $team_role1 = mysqli_real_escape_string($con,$_POST["team_role1"]);

    $team_name2 = mysqli_real_escape_string($con,$_POST["team_name2"]);
    $team_role2 = mysqli_real_escape_string($con,$_POST["team_role2"]);

    $team_name3 = mysqli_real_escape_string($con,$_POST["team_name3"]);
    $team_role3 = mysqli_real_escape_string($con,$_POST["team_role3"]);
}

    $happy_clients = mysqli_real_escape_string($con,$_POST["happy_clients"]);
    $peoples_love = mysqli_real_escape_string($con,$_POST["peoples_love"]);
    $gallery_description = mysqli_real_escape_string($con,$_POST["gallery_description"]);
    $place = mysqli_real_escape_string($con,$_POST["place"]);
    $zip = mysqli_real_escape_string($con,$_POST["zip"]);
    $hours = mysqli_real_escape_string($con,$_POST["hours"]);
    $phone = mysqli_real_escape_string($con,$_POST["phone"]);
    $email = mysqli_real_escape_string($con,$_POST["email"]);
    $address = mysqli_real_escape_string($con,$_POST["address"]);


  
    $statusresult = mysqli_query($con,"select * from spa_templates where id='$id'");
    $resultstatus = mysqli_fetch_array($statusresult);
    $status = $resultstatus["status"];
    $totalresult = mysqli_query($con,"select * from spa_templates");
    $imageresult = mysqli_fetch_array($totalresult);


   
    // logo
    if(empty($_FILES["image"]["name"]))
    {
      $logoname = $_POST["logo"];
    }
   
    else
    {
      $logoname = $_POST["logo"];
    
      if($status=="1") // for checking if it is a copied image
      {
        
          if(in_array($logoname,$imageresult)) // copied image and update first Time
          {
            
            $random = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyz"),0,7);
            $filename = $_FILES["image"]["name"];
            $tmp_name = $_FILES["image"]["tmp_name"];
            $logoname = $random.$filename;
            $logo = '../uploadedimages/'.$logoname;
            move_uploaded_file($tmp_name,$logo);
          }
          else // copied files and updated second time
          {
           
            $random = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyz"),0,7);
            $filename = $_FILES["image"]["name"];
            $tmp_name = $_FILES["image"]["tmp_name"];
            $logoname = $random.$filename;
            $logo = '../uploadedimages/'.$logoname;
            move_uploaded_file($tmp_name,$logo);
            $oldlogo = $_POST["logo"]; 
            $oldimageurl = '../uploadedimages/'.$oldlogo;    //// getting already existing file for unlinking
          
            if(file_exists("$oldimageurl"))
            {
              unlink($oldimageurl);
            }
            unset($oldlogo); // undefining that variable
          }
       }
       else
       {
         
          $random = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyz"),0,7);
          $filename = $_FILES["image"]["name"];
          $tmp_name = $_FILES["image"]["tmp_name"];
          $logoname = $random.$filename;
          $logo = '../uploadedimages/'.$logoname;
          move_uploaded_file($tmp_name,$logo);
          $oldlogo = $_POST["logo"];     //// getting already existing file for unlinking
          $oldimageurl = '../uploadedimages/'.$oldlogo;    //// getting already existing file for unlinking
         
          if(file_exists($oldimageurl))
          {
            unlink($oldimageurl);
          }
          unset($oldlogo); // undefining that variable
       }
    }
    
//service Information
// update without selecting a file
     if(empty($_FILES["service_icon1"]["name"]))
    {
      $service_icon1 = $_POST["service_icon1"];
    }
   
    else
    {
      $service_icon1 = $_POST["service_icon1"];
    
      if($status=="1") // for checking if it is a copied image
      {
        
          if(in_array($service_icon1,$imageresult)) // copied image and update first Time
          {
            
            $random = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyz"),0,7);
            $filename = $_FILES["service_icon1"]["name"];
            $tmp_name = $_FILES["service_icon1"]["tmp_name"];
            $service_icon1 = $random.$filename;
            $service = '../uploadedimages/'.$service_icon1;
            move_uploaded_file($tmp_name,$service);
          }
          else // copied files and updated second time
          {
           
            $random = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyz"),0,7);
            $filename = $_FILES["service_icon1"]["name"];
            $tmp_name = $_FILES["service_icon1"]["tmp_name"];
            $service_icon1 = $random.$service_icon1;
            $service = '../uploadedimages/'.$logoname;
            move_uploaded_file($tmp_name,$service);
            $oldservice_icon1 = $_POST["service_icon1"]; 
            $oldimageurl = '../uploadedimages/'. $oldservice_icon1;
             //// getting already existing file for unlinking
            if(file_exists($oldimageurl))
            {
              unlink( $oldimageurl);
            }
            unset( $oldservice_icon1); // undefining that variable
          }
       }
       else
       {
         
          $random = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyz"),0,7);
          $filename = $_FILES["service_icon1"]["name"];
          $tmp_name = $_FILES["service_icon1"]["tmp_name"];
          $service_icon1 = $random.$filename;
          $service = '../uploadedimages/'.$service_icon1;
          move_uploaded_file($tmp_name,$service);
          $oldservice_icon1 = $_POST["service_icon1"];     //// getting already existing file for unlinking
          $oldimageurl = '../uploadedimages/'. $oldservice_icon1; 
                  //// getting already existing file for unlinking
          if(file_exists($oldimageurl))
          {
            unlink($oldimageurl);
          }
          unset( $oldservice_icon1); // undefining that variable
       }
    }

// service_icon2
if(empty($_FILES["service_icon2"]["name"]))
{
 
$service_icon2 = $_POST["service_icon2"];

}

else
{
  $service_icon2 = $_POST["service_icon2"];

  if($status=="1") // for checking if it is a copied image
  {
    
      if(in_array($service_icon2,$imageresult)) // copied image and update first Time
      {
        
        $random = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyz"),0,7);
        $filename = $_FILES["service_icon2"]["name"];
        $tmp_name = $_FILES["service_icon2"]["tmp_name"];
        $service_icon2 = $random.$filename;
        $service2 = '../uploadedimages/'.$service_icon2;
        move_uploaded_file($tmp_name,$service2);
      }
      else // copied files and updated second time
      {
        
        $random = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyz"),0,7);
        $filename = $_FILES["service_icon2"]["name"];
        $tmp_name = $_FILES["service_icon2"]["tmp_name"];
        $service_icon2 = $random.$filename;
        $service2 = '../uploadedimages/'.$service_icon2;
        move_uploaded_file($tmp_name,$service2);
        $oldservice_icon2 = $_POST["service_icon2"]; 
        $oldimageurl2 = '../uploadedimages/'.$oldservice_icon2;
         //// getting already existing file for unlinking
        if(file_exists($oldimageurl2))
        {
          unlink($oldimageurl2);
        }
        unset( $oldservice_icon2); // undefining that variable
      }
   }
   else
   {
      
      $random = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyz"),0,7);
      $filename = $_FILES["service_icon2"]["name"];
      $tmp_name = $_FILES["service_icon2"]["tmp_name"];
      $service_icon2 = $random.$filename;
      
      $service2 = '../uploadedimages/'.$service_icon2;
      move_uploaded_file($tmp_name,$service2);
      $oldservice_icon2 = $_POST["service_icon2"];     //// getting already existing file for unlinking
      $oldimageurl2 = '../uploadedimages/'.$oldservice_icon2; 
              //// getting already existing file for unlinking
      if(file_exists($oldimageurl2))
      {
        unlink( $oldimageurl2);
      }
      unset($oldservice_icon2); // undefining that variable
   }
}
//service_icon2 ends

//feature_icon1 starts
if(empty($_FILES["feature_icon1"]["name"]))
{
 
  $feature_icon1 = $_POST["feature_icon1"];

}

else
{
  $feature_icon1 = $_POST["feature_icon1"];

  if($status=="1") // for checking if it is a copied image
  {
    
      if(in_array($feature_icon1,$imageresult)) // copied image and update first Time
      {
        
        $random = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyz"),0,7);
        $filename = $_FILES["feature_icon1"]["name"];
        $tmp_name = $_FILES["feature_icon1"]["tmp_name"];
        $feature_icon1 = $random.$filename;
        $feature1 = '../uploadedimages/'.$feature_icon1;
        move_uploaded_file($tmp_name,$feature1);
      }
      else // copied files and updated second time
      {
        
        $random = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyz"),0,7);
        $filename = $_FILES["feature_icon1"]["name"];
        $tmp_name = $_FILES["feature_icon1"]["tmp_name"];
        $feature_icon1 = $random.$filename;
        $feature1 = '../uploadedimages/'.$feature_icon1;
        move_uploaded_file($tmp_name,$feature1);
        $oldfeature_icon1 = $_POST["feature_icon1"]; 
        $oldfeatureurl1 = '../uploadedimages/'.$oldfeature_icon1;
         //// getting already existing file for unlinking
        if(file_exists($oldfeatureurl1))
        {
          unlink($oldfeatureurl1);
        }
        unset($oldfeature_icon1); // undefining that variable
      }
   }
   else
   {
      
      $random = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyz"),0,7);
      $filename = $_FILES["feature_icon1"]["name"];
      $tmp_name = $_FILES["feature_icon1"]["tmp_name"];
      $feature_icon1 = $random.$filename;
      $feature1 = '../uploadedimages/'.$feature_icon1;
      move_uploaded_file($tmp_name,$feature1);
      $oldfeature_icon1 = $_POST["feature_icon1"];     //// getting already existing file for unlinking
      $oldfeatureurl1 = '../uploadedimages/'.$oldfeature_icon1; 
              //// getting already existing file for unlinking
      if(file_exists($oldfeatureurl1))
      {
        unlink($oldfeatureurl1);
      }
      unset($oldfeature_icon1); // undefining that variable
   }
}

//feature_icon1 ends
//feature_icon2 starts
if(empty($_FILES["feature_icon2"]["name"]))
{
 
  $feature_icon2 = $_POST["feature_icon2"];

}

else
{
  $feature_icon2 = $_POST["feature_icon2"];

  if($status=="1") // for checking if it is a copied image
  {
    
      if(in_array($feature_icon2,$imageresult)) // copied image and update first Time
      {
        
        $random = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyz"),0,7);
        $filename = $_FILES["feature_icon2"]["name"];
        $tmp_name = $_FILES["feature_icon2"]["tmp_name"];
        $feature_icon2 = $random.$filename;
        $feature2 = '../uploadedimages/'.$feature_icon2;
        move_uploaded_file($tmp_name,$feature2);
      }
      else // copied files and updated second time
      {
      
        $random = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyz"),0,7);
        $filename = $_FILES["feature_icon2"]["name"];
        $tmp_name = $_FILES["feature_icon2"]["tmp_name"];
        $feature_icon2 = $random.$filename;
        $feature2 = '../uploadedimages/'.$feature_icon2;
        move_uploaded_file($tmp_name,$feature2);
        $oldfeature_icon2 = $_POST["feature_icon2"]; 
        $oldfeatureurl2 = '../uploadedimages/'.$oldfeature_icon2;
         //// getting already existing file for unlinking
        if(file_exists($oldfeatureurl2))
        {
          unlink($oldfeatureurl2);
        }
        unset($oldfeature_icon2); // undefining that variable
      }
   }
   else
   {
    
      $random = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyz"),0,7);
      $filename = $_FILES["feature_icon2"]["name"];
      $tmp_name = $_FILES["feature_icon2"]["tmp_name"];
      $feature_icon2 = $random.$filename;
      $feature2 = '../uploadedimages/'.$feature_icon2;
      move_uploaded_file($tmp_name,$feature2);
      $oldfeature_icon2 = $_POST["feature_icon2"];     //// getting already existing file for unlinking
      $oldfeatureurl2 = '../uploadedimages/'.$oldfeature_icon2; 
              //// getting already existing file for unlinking
      if(file_exists($oldfeatureurl2))
      {
        unlink($oldfeatureurl2);
      }
      unset($oldfeature_icon2); // undefining that variable
   }
}
//feature_icon2 ends

//feature_icon3 starts
if(empty($_FILES["feature_icon3"]["name"]))
{
 
  $feature_icon3 = $_POST["feature_icon3"];

}

else
{
  $feature_icon3 = $_POST["feature_icon3"];

  if($status=="1") // for checking if it is a copied image
  {
    
      if(in_array($feature_icon3,$imageresult)) // copied image and update first Time
      {
        
        $random = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyz"),0,7);
        $filename = $_FILES["feature_icon3"]["name"];
        $tmp_name = $_FILES["feature_icon3"]["tmp_name"];
        $feature_icon3 = $random.$filename;
        $feature3 = '../uploadedimages/'.$feature_icon3;
        move_uploaded_file($tmp_name,$feature3);
      }
      else // copied files and updated second time
      {
      
        $random = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyz"),0,7);
        $filename = $_FILES["feature_icon3"]["name"];
        $tmp_name = $_FILES["feature_icon3"]["tmp_name"];
        $feature_icon3 = $random.$filename;
        $feature3 = '../uploadedimages/'.$feature_icon3;
        move_uploaded_file($tmp_name,$feature3);
        $oldfeature_icon3 = $_POST["feature_icon3"]; 
        $oldfeatureurl3 = '../uploadedimages/'.$oldfeature_icon3;
         //// getting already existing file for unlinking
        if(file_exists($oldfeatureurl3))
        {
          unlink($oldfeatureurl3);
        }
        unset($oldfeature_icon3); // undefining that variable
      }
   }
   else
   {
    
      $random = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyz"),0,7);
      $filename = $_FILES["feature_icon3"]["name"];
      $tmp_name = $_FILES["feature_icon3"]["tmp_name"];
      $feature_icon3 = $random.$filename;
      $feature3 = '../uploadedimages/'.$feature_icon3;
      move_uploaded_file($tmp_name,$feature3);
      $oldfeature_icon3 = $_POST["feature_icon3"];     //// getting already existing file for unlinking
      $oldfeatureurl3 = '../uploadedimages/'.$oldfeature_icon3; 
              //// getting already existing file for unlinking
      if(file_exists($oldfeatureurl3))
      {
        unlink($oldfeatureurl3);
      }
      unset($oldfeature_icon3); // undefining that variable
   }
}
//feature_icon3 ends

//feature_icon4 starts
if(empty($_FILES["feature_icon4"]["name"]))
{
 
  $feature_icon4 = $_POST["feature_icon4"];

}

else
{
  $feature_icon4 = $_POST["feature_icon4"];

  if($status=="1") // for checking if it is a copied image
  {
    
      if(in_array($feature_icon4,$imageresult)) // copied image and update first Time
      {
        
        $random = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyz"),0,7);
        $filename = $_FILES["feature_icon4"]["name"];
        $tmp_name = $_FILES["feature_icon4"]["tmp_name"];
        $feature_icon4 = $random.$filename;
        $feature4 = '../uploadedimages/'.$feature_icon4;
        move_uploaded_file($tmp_name,$feature4);
      }
      else // copied files and updated second time
      {
      
        $random = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyz"),0,7);
        $filename = $_FILES["feature_icon4"]["name"];
        $tmp_name = $_FILES["feature_icon4"]["tmp_name"];
        $feature_icon4 = $random.$filename;
        $feature4 = '../uploadedimages/'.$feature_icon4;
        move_uploaded_file($tmp_name,$feature4);
        $oldfeature_icon4 = $_POST["feature_icon4"]; 
        $oldfeatureurl4 = '../uploadedimages/'.$oldfeature_icon4;
         //// getting already existing file for unlinking
        if(file_exists($oldfeatureurl4))
        {
          unlink($oldfeatureurl4);
        }
        unset($oldfeature_icon4); // undefining that variable
      }
   }
   else
   {
     
      $random = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyz"),0,7);
      $filename = $_FILES["feature_icon4"]["name"];
      $tmp_name = $_FILES["feature_icon4"]["tmp_name"];
      $feature_icon4 = $random.$filename;
      $feature4 = '../uploadedimages/'.$feature_icon4;
      move_uploaded_file($tmp_name,$feature4);
      $oldfeature_icon4 = $_POST["feature_icon4"];     //// getting already existing file for unlinking
      $oldfeatureurl4 = '../uploadedimages/'.$oldfeature_icon4; 
              //// getting already existing file for unlinking
      if(file_exists($oldfeatureurl4))
      {
        unlink($oldfeatureurl4);
      }
      unset($oldfeature_icon4); // undefining that variable
   }
}
//feature_icon4 ends

//feature_icon5 starts
if(empty($_FILES["feature_icon5"]["name"]))
{
 
  $feature_icon5 = $_POST["feature_icon5"];

}

else
{
  $feature_icon5 = $_POST["feature_icon5"];

  if($status=="1") // for checking if it is a copied image
  {
    
      if(in_array($feature_icon5,$imageresult)) // copied image and update first Time
      {
        
        $random = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyz"),0,7);
        $filename = $_FILES["feature_icon5"]["name"];
        $tmp_name = $_FILES["feature_icon5"]["tmp_name"];
        $feature_icon5 = $random.$filename;
        $feature5 = '../uploadedimages/'.$feature_icon5;
        move_uploaded_file($tmp_name,$feature5);
      }
      else // copied files and updated second time
      {
      
        $random = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyz"),0,7);
        $filename = $_FILES["feature_icon5"]["name"];
        $tmp_name = $_FILES["feature_icon5"]["tmp_name"];
        $feature_icon5 = $random.$filename;
        $feature5 = '../uploadedimages/'.$feature_icon5;
        move_uploaded_file($tmp_name,$feature5);
        $oldfeature_icon5 = $_POST["feature_icon5"]; 
        $oldfeatureurl5 = '../uploadedimages/'.$oldfeature_icon5;
         //// getting already existing file for unlinking
        if(file_exists($oldfeatureurl5))
        {
          unlink($oldfeatureurl5);
        }
        unset($oldfeature_icon5); // undefining that variable
      }
   }
   else
   {
     
      $random = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyz"),0,7);
      $filename = $_FILES["feature_icon5"]["name"];
      $tmp_name = $_FILES["feature_icon5"]["tmp_name"];
      $feature_icon5 = $random.$filename;
      $feature5 = '../uploadedimages/'.$feature_icon5;
      move_uploaded_file($tmp_name,$feature5);
      $oldfeature_icon5 = $_POST["feature_icon5"];     //// getting already existing file for unlinking
      $oldfeatureurl5 = '../uploadedimages/'.$oldfeature_icon5; 
              //// getting already existing file for unlinking
      if(file_exists($oldfeatureurl5))
      {
        unlink($oldfeatureurl5);
      }
      unset($oldfeature_icon5); // undefining that variable
   }
}
//feature_icon5 ends


//feature_icon6 starts
if(empty($_FILES["feature_icon6"]["name"]))
{
 
  $feature_icon6 = $_POST["feature_icon6"];

}

else
{
  $feature_icon6 = $_POST["feature_icon6"];

  if($status=="1") // for checking if it is a copied image
  {
    
      if(in_array($feature_icon6,$imageresult)) // copied image and update first Time
      {
        
        $random = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyz"),0,7);
        $filename = $_FILES["feature_icon6"]["name"];
        $tmp_name = $_FILES["feature_icon6"]["tmp_name"];
        $feature_icon6 = $random.$filename;
        $feature6 = '../uploadedimages/'.$feature_icon6;
        move_uploaded_file($tmp_name,$feature6);
      }
      else // copied files and updated second time
      {
      
        $random = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyz"),0,7);
        $filename = $_FILES["feature_icon6"]["name"];
        $tmp_name = $_FILES["feature_icon6"]["tmp_name"];
        $feature_icon6 = $random.$filename;
        $feature6 = '../uploadedimages/'.$feature_icon6;
        move_uploaded_file($tmp_name,$feature6);
        $oldfeature_icon6 = $_POST["feature_icon6"]; 
        $oldfeatureurl6 = '../uploadedimages/'.$oldfeature_icon6;
         //// getting already existing file for unlinking
        if(file_exists($oldfeatureurl6))
        {
          unlink($oldfeatureurl6);
        }
        unset($oldfeature_icon6); // undefining that variable
      }
   }
   else
   {
     
      $random = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyz"),0,7);
      $filename = $_FILES["feature_icon6"]["name"];
      $tmp_name = $_FILES["feature_icon6"]["tmp_name"];
      $feature_icon6 = $random.$filename;
      $feature6 = '../uploadedimages/'.$feature_icon6;
      move_uploaded_file($tmp_name,$feature6);
      $oldfeature_icon6 = $_POST["feature_icon6"];     //// getting already existing file for unlinking
      $oldfeatureurl6 = '../uploadedimages/'.$oldfeature_icon6; 
              //// getting already existing file for unlinking
      if(file_exists($oldfeatureurl6))
      {
        unlink($oldfeatureurl6);
      }
      unset($oldfeature_icon6); // undefining that variable
   }
}
//feature_icon6 ends



if($theme=="1")
{
//testimonial_icon1 starts
if(empty($_FILES["testimonial_icon1"]["name"]))
{
 
  $testimonial_icon1 = $_POST["testimonial_icon1"];

}

else
{
  $testimonial_icon1 = $_POST["testimonial_icon1"];

  if($status=="1") // for checking if it is a copied image
  {
    
      if(in_array($testimonial_icon1,$imageresult)) // copied image and update first Time
      {
        
        $random = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyz"),0,7);
        $filename = $_FILES["testimonial_icon1"]["name"];
        $tmp_name = $_FILES["testimonial_icon1"]["tmp_name"];
        $testimonial_icon1 = $random.$filename;
        $testimonialicon1 = '../uploadedimages/'.$testimonial_icon1;
        move_uploaded_file($tmp_name, $testimonialicon1);
      }
      else // copied files and updated second time
      {
       
     
        $random = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyz"),0,7);
        $filename = $_FILES["testimonial_icon1"]["name"];
        $tmp_name = $_FILES["testimonial_icon1"]["tmp_name"];
        $testimonial_icon1 = $random.$filename;
       
        $testimonialicon1 = '../uploadedimages/'.$testimonial_icon1;
        move_uploaded_file($tmp_name,$testimonialicon1);
        $oldtestimonial_icon1 = $_POST["testimonial_icon1"]; 
        $oldtestimonial_iconurl1 = '../uploadedimages/'.$oldtestimonial_icon1;
         //// getting already existing file for unlinking
        if(file_exists($oldtestimonial_iconurl1))
        {
          unlink($oldtestimonial_iconurl1);
        }
        unset($oldtestimonial_icon1); // undefining that variable
      }
   }
   else
   {
      $random = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyz"),0,7);
      $filename = $_FILES["testimonial_icon1"]["name"];
      $tmp_name = $_FILES["testimonial_icon1"]["tmp_name"];
      $testimonial_icon1 = $random.$filename;
      
      $testimonialicon1 = '../uploadedimages/'.$testimonial_icon1;
      move_uploaded_file($tmp_name, $testimonialicon1);
      $oldtestimonial_icon1 = $_POST["testimonial_icon1"];     //// getting already existing file for unlinking
      $oldtestimonial_iconurl1 = '../uploadedimages/'.$oldtestimonial_icon1; 
              //// getting already existing file for unlinking
      if(file_exists($oldtestimonial_iconurl1))
      {
        unlink($oldtestimonial_iconurl1);
      }
      unset($oldtestimonial_icon1); // undefining that variable
   }
}



//testimonial_icon1 ends

//testimonial_icon2 starts
if(empty($_FILES["testimonial_icon2"]["name"]))
{
 
  $testimonial_icon2 = $_POST["testimonial_icon2"];

}

else
{
  $testimonial_icon2 = $_POST["testimonial_icon2"];

  if($status=="1") // for checking if it is a copied image
  {
    
      if(in_array($testimonial_icon2,$imageresult)) // copied image and update first Time
      {
        
        $random = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyz"),0,7);
        $filename = $_FILES["testimonial_icon2"]["name"];
        $tmp_name = $_FILES["testimonial_icon2"]["tmp_name"];
        $testimonial_icon2 = $random.$filename;
        $testimonialicon2 = '../uploadedimages/'.$testimonial_icon2;
        move_uploaded_file($tmp_name,$testimonialicon2);
      }
      else // copied files and updated second time
      {
       
     
        $random = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyz"),0,7);
        $filename = $_FILES["testimonial_icon2"]["name"];
        $tmp_name = $_FILES["testimonial_icon2"]["tmp_name"];
        $testimonial_icon2 = $random.$filename;
        $testimonialicon2 = '../uploadedimages/'.$testimonial_icon2;
        move_uploaded_file($tmp_name,$testimonialicon2);
        $oldtestimonial_icon2 = $_POST["testimonial_icon2"]; 
        $oldtestimonial_iconurl2 = '../uploadedimages/'.$oldtestimonial_icon2;
         //// getting already existing file for unlinking
        if(file_exists($oldtestimonial_iconurl2))
        {
          unlink( $oldtestimonial_iconurl2);
        }
        unset($oldtestimonial_icon2); // undefining that variable
      }
   }
   else
   {
      $random = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyz"),0,7);
      $filename = $_FILES["testimonial_icon2"]["name"];
      $tmp_name = $_FILES["testimonial_icon2"]["tmp_name"];
      $testimonial_icon2 = $random.$filename;
      
      $testimonialicon2 = '../uploadedimages/'.$testimonial_icon2;
      move_uploaded_file($tmp_name,$testimonialicon2);
      $oldtestimonial_icon2 = $_POST["testimonial_icon2"];     //// getting already existing file for unlinking
      $oldtestimonial_iconurl2 = '../uploadedimages/'.$oldtestimonial_icon2; 
              //// getting already existing file for unlinking
      if(file_exists($oldtestimonial_iconurl2))
      {
        unlink($oldtestimonial_iconurl2);
      }
      unset($oldtestimonial_icon2); // undefining that variable
   }
}
//testimonial_icon2 ends

//testimonial_icon3 starts
if(empty($_FILES["testimonial_icon3"]["name"]))
{
 
  $testimonial_icon3 = $_POST["testimonial_icon3"];

}

else
{
  $testimonial_icon3 = $_POST["testimonial_icon3"];

  if($status=="1") // for checking if it is a copied image
  {
    
      if(in_array($testimonial_icon3,$imageresult)) // copied image and update first Time
      {
        
        $random = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyz"),0,7);
        $filename = $_FILES["testimonial_icon3"]["name"];
        $tmp_name = $_FILES["testimonial_icon3"]["tmp_name"];
        $testimonial_icon3 = $random.$filename;
        $testimonialicon3 = '../uploadedimages/'.$testimonial_icon3;
        move_uploaded_file($tmp_name,$testimonialicon3);
      }
      else // copied files and updated second time
      {
       
     
        $random = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyz"),0,7);
        $filename = $_FILES["testimonial_icon3"]["name"];
        $tmp_name = $_FILES["testimonial_icon3"]["tmp_name"];
        $testimonial_icon3 = $random.$filename;
        $testimonialicon3 = '../uploadedimages/'.$testimonial_icon3;
        move_uploaded_file($tmp_name,$testimonialicon3);
        $oldtestimonial_icon3 = $_POST["testimonial_icon2"]; 
        $oldtestimonial_iconurl3 = '../uploadedimages/'.$oldtestimonial_icon3;
         //// getting already existing file for unlinking
        if(file_exists($oldtestimonial_iconurl3))
        {
          unlink($oldtestimonial_iconurl3);
        }
        unset($oldtestimonial_icon3); // undefining that variable
      }
   }
   else
   {
      $random = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyz"),0,7);
      $filename = $_FILES["testimonial_icon3"]["name"];
      $tmp_name = $_FILES["testimonial_icon3"]["tmp_name"];
      $testimonial_icon3 = $random.$filename;
      
      $testimonialicon3 = '../uploadedimages/'.$testimonial_icon3;
      move_uploaded_file($tmp_name,$testimonialicon3);
      $oldtestimonial_icon3 = $_POST["testimonial_icon3"];     //// getting already existing file for unlinking
      $oldtestimonial_iconurl3 = '../uploadedimages/'.$oldtestimonial_icon3; 
              //// getting already existing file for unlinking
      if(file_exists($oldtestimonial_iconurl3))
      {
        unlink($oldtestimonial_iconurl3);
      }
      unset($oldtestimonial_icon3); // undefining that variable
   }
}
//testimonial_icon3 ends

//testimonial_icon4 starts
if(empty($_FILES["testimonial_icon4"]["name"]))
{
 
  $testimonial_icon4 = $_POST["testimonial_icon4"];

}

else
{
  $testimonial_icon4 = $_POST["testimonial_icon4"];

  if($status=="1") // for checking if it is a copied image
  {
    
      if(in_array($testimonial_icon4,$imageresult)) // copied image and update first Time
      {
        
        $random = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyz"),0,7);
        $filename = $_FILES["testimonial_icon4"]["name"];
        $tmp_name = $_FILES["testimonial_icon4"]["tmp_name"];
        $testimonial_icon4 = $random.$filename;
        $testimonialicon4 = '../uploadedimages/'.$testimonial_icon4;
        move_uploaded_file($tmp_name,$testimonialicon4);
      }
      else // copied files and updated second time
      {
       
     
        $random = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyz"),0,7);
        $filename = $_FILES["testimonial_icon4"]["name"];
        $tmp_name = $_FILES["testimonial_icon4"]["tmp_name"];
        $testimonial_icon4 = $random.$filename;
       
        $testimonialicon4 = '../uploadedimages/'.$testimonial_icon4;
        move_uploaded_file($tmp_name,$testimonialicon4);
        $oldtestimonial_icon4 = $_POST["testimonial_icon4"]; 
        $oldtestimonial_iconurl4 = '../uploadedimages/'.$oldtestimonial_icon4;
         //// getting already existing file for unlinking
        if(file_exists($oldtestimonial_iconurl4))
        {
          unlink($oldtestimonial_iconurl4);
        }
        unset($oldtestimonial_icon4); // undefining that variable
      }
   }
   else
   {
      $random = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyz"),0,7);
      $filename = $_FILES["testimonial_icon4"]["name"];
      $tmp_name = $_FILES["testimonial_icon4"]["tmp_name"];
      $testimonial_icon4 = $random.$filename;
      
      $testimonialicon4 = '../uploadedimages/'.$testimonial_icon4;
      move_uploaded_file($tmp_name, $testimonialicon4);
      $oldtestimonial_icon4 = $_POST["testimonial_icon4"];     //// getting already existing file for unlinking
      $oldtestimonial_iconurl4 = '../uploadedimages/'.$oldtestimonial_icon4; 
              //// getting already existing file for unlinking
      if(file_exists($oldtestimonial_iconurl4))
      {
        unlink($oldtestimonial_iconurl4);
      }
      unset($oldtestimonial_icon4); // undefining that variable
   }
}
}

if($theme=="2")
{

  //service icon 3
  if(empty($_FILES["service_icon3"]["name"]))
{
 
$service_icon3 = $_POST["service_icon3"];

}

else
{
  $service_icon3 = $_POST["service_icon3"];

  if($status=="1") // for checking if it is a copied image
  {
    
      if(in_array($service_icon3,$imageresult)) // copied image and update first Time
      {
        
        $random = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyz"),0,7);
        $filename = $_FILES["service_icon3"]["name"];
        $tmp_name = $_FILES["service_icon3"]["tmp_name"];
        $service_icon3 = $random.$filename;
        $service3 = '../uploadedimages/'.$service_icon3;
        move_uploaded_file($tmp_name,$service3);
      }
      else // copied files and updated second time
      {
        
        $random = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyz"),0,7);
        $filename = $_FILES["service_icon3"]["name"];
        $tmp_name = $_FILES["service_icon3"]["tmp_name"];
        $service_icon3 = $random.$filename;
        $service3 = '../uploadedimages/'.$service_icon3;
        move_uploaded_file($tmp_name,$service3);
        $oldservice_icon3 = $_POST["service_icon3"]; 
        $oldimageurl3 = '../uploadedimages/'.$oldservice_icon3;
         //// getting already existing file for unlinking
        if(file_exists($oldimageurl3))
        {
          unlink($oldimageurl3);
        }
        unset( $oldservice_icon3); // undefining that variable
      }
   }
   else
   {
      
      $random = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyz"),0,7);
      $filename = $_FILES["service_icon3"]["name"];
      $tmp_name = $_FILES["service_icon3"]["tmp_name"];
      $service_icon3 = $random.$filename;
      
      $service3 = '../uploadedimages/'.$service_icon3;
      move_uploaded_file($tmp_name,$service3);
      $oldservice_icon3 = $_POST["service_icon3"];     //// getting already existing file for unlinking
      $oldimageurl3 = '../uploadedimages/'.$oldservice_icon3; 
              //// getting already existing file for unlinking
      if(file_exists($oldimageurl3))
      {
        unlink( $oldimageurl3);
      }
      unset($oldservice_icon3); // undefining that variable
   }
}
//team_image1 starts
if(empty($_FILES["team_image1"]["name"]))
{
 
  $team_image1 = $_POST["team_image1"];

}

else
{
  $team_image1 = $_POST["team_image1"];

  if($status=="1") // for checking if it is a copied image
  {
    
      if(in_array($team_image1,$imageresult)) // copied image and update first Time
      {
        
        $random = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyz"),0,7);
        $filename = $_FILES["team_image1"]["name"];
        $tmp_name = $_FILES["team_image1"]["tmp_name"];
        $team_image1 = $random.$filename;
        $teamimage1 = '../uploadedimages/'.$team_image1;
        move_uploaded_file($tmp_name, $teamimage1);
      }
      else // copied files and updated second time
      {
       
     
        $random = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyz"),0,7);
        $filename = $_FILES["team_image1"]["name"];
        $tmp_name = $_FILES["team_image1"]["tmp_name"];
        $team_image1 = $random.$filename;
       
        $teamimage1 = '../uploadedimages/'.$team_image1;
        move_uploaded_file($tmp_name,$teamimage1);
        $oldteam_image1 = $_POST["team_image1"]; 
        $oldteam_imageurl1 = '../uploadedimages/'.$oldteam_image1;
         //// getting already existing file for unlinking
        if(file_exists($oldteam_imageurl1))
        {
          unlink($oldteam_imageurl1);
        }
        unset($oldteam_image1); // undefining that variable
      }
   }
   else
   {
      $random = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyz"),0,7);
      $filename = $_FILES["team_image1"]["name"];
      $tmp_name = $_FILES["team_image1"]["tmp_name"];
      $team_image1 = $random.$filename;
      
      $teamimage1 = '../uploadedimages/'.$team_image1;
      move_uploaded_file($tmp_name, $teamimage1);
      $oldteam_image1 = $_POST["team_image1"];     //// getting already existing file for unlinking
      $oldteam_imageurl1 = '../uploadedimages/'.$oldteam_image1; 
              //// getting already existing file for unlinking
      if(file_exists($oldteam_imageurl1))
      {
        unlink($oldteam_imageurl1);
      }
      unset($oldteam_image1); // undefining that variable
   }
}



//team_image1 ends


//team_image2 starts
if(empty($_FILES["team_image2"]["name"]))
{
 
  $team_image2 = $_POST["team_image2"];

}

else
{
  $team_image2 = $_POST["team_image2"];

  if($status=="1") // for checking if it is a copied image
  {
    
      if(in_array($team_image2,$imageresult)) // copied image and update first Time
      {
        
        $random = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyz"),0,7);
        $filename = $_FILES["team_image2"]["name"];
        $tmp_name = $_FILES["team_image2"]["tmp_name"];
        $team_image2 = $random.$filename;
        $teamimage2 = '../uploadedimages/'.$team_image2;
        move_uploaded_file($tmp_name, $teamimage2);
      }
      else // copied files and updated second time
      {
       
     
        $random = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyz"),0,7);
        $filename = $_FILES["team_image2"]["name"];
        $tmp_name = $_FILES["team_image2"]["tmp_name"];
        $team_image2 = $random.$filename;
       
        $teamimage2 = '../uploadedimages/'.$team_image2;
        move_uploaded_file($tmp_name,$teamimage2);
        $oldteam_image2 = $_POST["team_image2"]; 
        $oldteam_imageurl2 = '../uploadedimages/'.$oldteam_image2;
         //// getting already existing file for unlinking
        if(file_exists($oldteam_imageurl2))
        {
          unlink($oldteam_imageurl2);
        }
        unset($oldteam_image2); // undefining that variable
      }
   }
   else
   {
      $random = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyz"),0,7);
      $filename = $_FILES["team_image2"]["name"];
      $tmp_name = $_FILES["team_image2"]["tmp_name"];
      $team_image2 = $random.$filename;
      
      $teamimage2 = '../uploadedimages/'.$team_image2;
      move_uploaded_file($tmp_name, $teamimage2);
      $oldteam_image2 = $_POST["team_image2"];     //// getting already existing file for unlinking
      $oldteam_imageurl2 = '../uploadedimages/'.$oldteam_image2; 
              //// getting already existing file for unlinking
      if(file_exists($oldteam_imageurl2))
      {
        unlink($oldteam_imageurl2);
      }
      unset($oldteam_image2); // undefining that variable
   }
}



//team_image2 ends


//team_image3 starts
if(empty($_FILES["team_image3"]["name"]))
{
 
  $team_image3 = $_POST["team_image3"];

}

else
{
  $team_image3 = $_POST["team_image3"];

  if($status=="1") // for checking if it is a copied image
  {
    
      if(in_array($team_image3,$imageresult)) // copied image and update first Time
      {
        
        $random = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyz"),0,7);
        $filename = $_FILES["team_image3"]["name"];
        $tmp_name = $_FILES["team_image3"]["tmp_name"];
        $team_image3 = $random.$filename;
        $teamimage3 = '../uploadedimages/'.$team_image3;
        move_uploaded_file($tmp_name, $teamimage3);
      }
      else // copied files and updated second time
      {
       
     
        $random = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyz"),0,7);
        $filename = $_FILES["team_image3"]["name"];
        $tmp_name = $_FILES["team_image3"]["tmp_name"];
        $team_image3 = $random.$filename;
       
        $teamimage3 = '../uploadedimages/'.$team_image3;
        move_uploaded_file($tmp_name,$teamimage3);
        $oldteam_image3 = $_POST["team_image3"]; 
        $oldteam_imageurl3 = '../uploadedimages/'.$oldteam_image3;
         //// getting already existing file for unlinking
        if(file_exists($oldteam_imageurl3))
        {
          unlink($oldteam_imageurl3);
        }
        unset($oldteam_image3); // undefining that variable
      }
   }
   else
   {
      $random = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyz"),0,7);
      $filename = $_FILES["team_image3"]["name"];
      $tmp_name = $_FILES["team_image3"]["tmp_name"];
      $team_image3 = $random.$filename;
      
      $teamimage3 = '../uploadedimages/'.$team_image3;
      move_uploaded_file($tmp_name, $teamimage3);
      $oldteam_image3 = $_POST["team_image3"];     //// getting already existing file for unlinking
      $oldteam_imageurl3 = '../uploadedimages/'.$oldteam_image3; 
              //// getting already existing file for unlinking
      if(file_exists($oldteam_imageurl3))
      {
        unlink($oldteam_imageurl3);
      }
      unset($oldteam_image3); // undefining that variable
   }
}



//team_image3 ends


//gallery_img7 starts

if(empty($_FILES["gallery_img7"]["name"]))
{
 
  $gallery_img7 = $_POST["gallery_img7"];

}

else
{
  $gallery_img7 = $_POST["gallery_img7"];

  if($status=="1") // for checking if it is a copied image
  {
    
      if(in_array($gallery_img7,$imageresult)) // copied image and update first Time
      {
        
        $random = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyz"),0,7);
        $filename = $_FILES["gallery_img7"]["name"];
        $tmp_name = $_FILES["gallery_img7"]["tmp_name"];
        $gallery_img7 = $random.$filename;
        $galleryimage7 = '../uploadedimages/'.$gallery_img7;
        move_uploaded_file($tmp_name,$galleryimage7);
      }
      else // copied files and updated second time
      {
       
        $random = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyz"),0,7);
        $filename = $_FILES["gallery_img7"]["name"];
        $tmp_name = $_FILES["gallery_img7"]["tmp_name"];
        $gallery_img7 = $random.$filename;
        
        $galleryimage7 = '../uploadedimages/'.$gallery_img7;
        move_uploaded_file($tmp_name,$galleryimage7);
        $oldgallery_img7 = $_POST["gallery_img7"]; 
        $oldgalleryimage_url7 = '../uploadedimages/'.$oldgallery_img7;
         //// getting already existing file for unlinking
        if(file_exists($oldgalleryimage_url7))
        {
          unlink($oldgalleryimage_url7);
        }
        unset($oldgallery_img7); // undefining that variable
      }
   }
   else
   {
     
      $random = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyz"),0,7);
      $filename = $_FILES["gallery_img7"]["name"];
      $tmp_name = $_FILES["gallery_img7"]["tmp_name"];
      $gallery_img7 = $random.$filename;
      
      $galleryimage6 = '../uploadedimages/'.$gallery_img7;
      move_uploaded_file($tmp_name,$galleryimage6);
      $oldgallery_img7 = $_POST["gallery_img7"];     //// getting already existing file for unlinking
      $oldgalleryimage_url7 = '../uploadedimages/'. $oldgallery_img7; 
              //// getting already existing file for unlinking
      if(file_exists($oldgalleryimage_url7))
      {
        unlink($oldgalleryimage_url7);
      }
      unset($oldgallery_img7); // undefining that variable
   }
}

//gallery_img7 ends

//gallery_img8 starts

if(empty($_FILES["gallery_img8"]["name"]))
{
 
  $gallery_img8 = $_POST["gallery_img8"];

}

else
{
  $gallery_img8 = $_POST["gallery_img8"];

  if($status=="1") // for checking if it is a copied image
  {
    
      if(in_array($gallery_img8,$imageresult)) // copied image and update first Time
      {
        
        $random = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyz"),0,7);
        $filename = $_FILES["gallery_img8"]["name"];
        $tmp_name = $_FILES["gallery_img8"]["tmp_name"];
        $gallery_img8 = $random.$filename;
        $galleryimage8 = '../uploadedimages/'.$gallery_img8;
        move_uploaded_file($tmp_name,$galleryimage8);
      }
      else // copied files and updated second time
      {
       
        $random = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyz"),0,7);
        $filename = $_FILES["gallery_img8"]["name"];
        $tmp_name = $_FILES["gallery_img8"]["tmp_name"];
        $gallery_img8 = $random.$filename;
        
        $galleryimage8 = '../uploadedimages/'.$gallery_img8;
        move_uploaded_file($tmp_name,$galleryimage8);
        $oldgallery_img8 = $_POST["gallery_img8"]; 
        $oldgalleryimage_url8 = '../uploadedimages/'.$oldgallery_img8;
         //// getting already existing file for unlinking
        if(file_exists($oldgalleryimage_url8))
        {
          unlink($oldgalleryimage_url8);
        }
        unset($oldgallery_img8); // undefining that variable
      }
   }
   else
   {
     
      $random = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyz"),0,7);
      $filename = $_FILES["gallery_img8"]["name"];
      $tmp_name = $_FILES["gallery_img8"]["tmp_name"];
      $gallery_img8 = $random.$filename;
      
      $galleryimage8 = '../uploadedimages/'.$gallery_img8;
      move_uploaded_file($tmp_name,$galleryimage8);
      $oldgallery_img8 = $_POST["gallery_img8"];     //// getting already existing file for unlinking
      $oldgalleryimage_url8 = '../uploadedimages/'. $oldgallery_img8; 
              //// getting already existing file for unlinking
      if(file_exists($oldgalleryimage_url8))
      {
        unlink($oldgalleryimage_url8);
      }
      unset($oldgallery_img8); // undefining that variable
   }
}

//gallery_img8 ends






}

//gallery_img1 starts
if(empty($_FILES["gallery_img1"]["name"]))
{
 
  $gallery_img1 = $_POST["gallery_img1"];

}

else
{
  $gallery_img1 = $_POST["gallery_img1"];

  if($status=="1") // for checking if it is a copied image
  {
    
      if(in_array($testimonial_icon4,$imageresult)) // copied image and update first Time
      {
        
        $random = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyz"),0,7);
        $filename = $_FILES["gallery_img1"]["name"];
        $tmp_name = $_FILES["gallery_img1"]["tmp_name"];
        $gallery_img1 = $random.$filename;
        $galleryimage1 = '../uploadedimages/'.$gallery_img1;
        move_uploaded_file($tmp_name,$galleryimage1);
      }
      else // copied files and updated second time
      {
       
        $random = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyz"),0,7);
        $filename = $_FILES["gallery_img1"]["name"];
        $tmp_name = $_FILES["gallery_img1"]["tmp_name"];
        $gallery_img1 = $random.$filename;
       
        $galleryimage1 = '../uploadedimages/'.$gallery_img1;
        move_uploaded_file($tmp_name,$galleryimage1);
        $oldgallery_img1 = $_POST["gallery_img1"]; 
        $oldgalleryimage_url1 = '../uploadedimages/'.$oldgallery_img1;
         //// getting already existing file for unlinking
        if(file_exists($oldgalleryimage_url1))
        {
          unlink($oldgalleryimage_url1);
        }
        unset($oldgallery_img1); // undefining that variable
      }
   }
   else
   {
      $random = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyz"),0,7);
      $filename = $_FILES["gallery_img1"]["name"];
      $tmp_name = $_FILES["gallery_img1"]["tmp_name"];
      $gallery_img1 = $random.$filename;
      
      $testimonialicon4 = '../uploadedimages/'.$gallery_img1;
      move_uploaded_file($tmp_name, $testimonialicon4);
      $oldgallery_img1 = $_POST["gallery_img1"];     //// getting already existing file for unlinking
      $oldgalleryimage_url1 = '../uploadedimages/'.$oldgallery_img1; 
              //// getting already existing file for unlinking
      if(file_exists($oldgalleryimage_url1))
      {
        unlink($oldgalleryimage_url1);
      }
      unset($oldgallery_img1); // undefining that variable
   }
}

//gallery_img1 ends
//gallery_img2  starts
if(empty($_FILES["gallery_img2"]["name"]))
{
 
  $gallery_img2 = $_POST["gallery_img2"];

}

else
{
  $gallery_img2 = $_POST["gallery_img2"];

  if($status=="1") // for checking if it is a copied image
  {
    
      if(in_array($gallery_img2,$imageresult)) // copied image and update first Time
      {
        
        $random = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyz"),0,7);
        $filename = $_FILES["gallery_img2"]["name"];
        $tmp_name = $_FILES["gallery_img2"]["tmp_name"];
        $gallery_img2 = $random.$filename;
        $galleryimage2 = '../uploadedimages/'.$gallery_img2;
        move_uploaded_file($tmp_name,$galleryimage2);
      }
      else // copied files and updated second time
      {
       
        $random = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyz"),0,7);
        $filename = $_FILES["gallery_img2"]["name"];
        $tmp_name = $_FILES["gallery_img2"]["tmp_name"];
        $gallery_img2 = $random.$filename;
       
        $galleryimage2 = '../uploadedimages/'.$gallery_img2;
        move_uploaded_file($tmp_name,$galleryimage2);
        $oldgallery_img2 = $_POST["gallery_img2"]; 
        $oldgalleryimage_url2 = '../uploadedimages/'.$oldgallery_img2;
         //// getting already existing file for unlinking
        if(file_exists($oldgalleryimage_url2))
        {
          unlink($oldgalleryimage_url2);
        }
        unset($oldgallery_img2); // undefining that variable
      }
   }
   else
   {
      $random = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyz"),0,7);
      $filename = $_FILES["gallery_img2"]["name"];
      $tmp_name = $_FILES["gallery_img2"]["tmp_name"];
      $gallery_img2 = $random.$filename;
      
      $galleryimage2 = '../uploadedimages/'.$gallery_img2;
      move_uploaded_file($tmp_name,$galleryimage2);
      $oldgallery_img2 = $_POST["gallery_img2"];     //// getting already existing file for unlinking
      $oldgalleryimage_url2 = '../uploadedimages/'. $oldgallery_img2; 
              //// getting already existing file for unlinking
      if(file_exists($oldgalleryimage_url2))
      {
        unlink($oldgalleryimage_url2);
      }
      unset($oldgallery_img2); // undefining that variable
   }
}
//gallery_img2 ends
//gallery_img3 starts

if(empty($_FILES["gallery_img3"]["name"]))
{
 
  $gallery_img3 = $_POST["gallery_img3"];

}

else
{
  $gallery_img3 = $_POST["gallery_img3"];

  if($status=="1") // for checking if it is a copied image
  {
    
      if(in_array($gallery_img3,$imageresult)) // copied image and update first Time
      {
        
        $random = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyz"),0,7);
        $filename = $_FILES["gallery_img3"]["name"];
        $tmp_name = $_FILES["gallery_img3"]["tmp_name"];
        $gallery_img3 = $random.$filename;
        $galleryimage3 = '../uploadedimages/'.$gallery_img3;
        move_uploaded_file($tmp_name, $galleryimage3);
      }
      else // copied files and updated second time
      {
       
        $random = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyz"),0,7);
        $filename = $_FILES["gallery_img3"]["name"];
        $tmp_name = $_FILES["gallery_img3"]["tmp_name"];
        $gallery_img3 = $random.$filename;
       
        $galleryimage3 = '../uploadedimages/'.$gallery_img3;
        move_uploaded_file($tmp_name,$galleryimage3);
        $oldgallery_img3 = $_POST["gallery_img3"]; 
        $oldgalleryimage_url3 = '../uploadedimages/'.$oldgallery_img3;
         //// getting already existing file for unlinking
        if(file_exists($oldgalleryimage_url3))
        {
          unlink($oldgalleryimage_url3);
        }
        unset($oldgallery_img3); // undefining that variable
      }
   }
   else
   {
      $random = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyz"),0,7);
      $filename = $_FILES["gallery_img3"]["name"];
      $tmp_name = $_FILES["gallery_img3"]["tmp_name"];
      $gallery_img3 = $random.$filename;
      
      $galleryimage3 = '../uploadedimages/'.$gallery_img3;
      move_uploaded_file($tmp_name,$galleryimage3);
      $oldgallery_img3 = $_POST["gallery_img3"];     //// getting already existing file for unlinking
      $oldgalleryimage_url3 = '../uploadedimages/'. $oldgallery_img3; 
              //// getting already existing file for unlinking
      if(file_exists($oldgalleryimage_url3))
      {
        unlink($oldgalleryimage_url3);
      }
      unset($oldgallery_img3); // undefining that variable
   }
}
//gallery_img3 ends
//gallery_img4 starts
if(empty($_FILES["gallery_img4"]["name"]))
{
 
  $gallery_img4 = $_POST["gallery_img4"];

}

else
{
  $gallery_img4 = $_POST["gallery_img4"];

  if($status=="1") // for checking if it is a copied image
  {
    
      if(in_array($gallery_img4,$imageresult)) // copied image and update first Time
      {
        
        $random = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyz"),0,7);
        $filename = $_FILES["gallery_img4"]["name"];
        $tmp_name = $_FILES["gallery_img4"]["tmp_name"];
        $gallery_img4 = $random.$filename;
        $galleryimage4 = '../uploadedimages/'.$gallery_img4;
        move_uploaded_file($tmp_name,$galleryimage4);
      }
      else // copied files and updated second time
      {
       
        $random = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyz"),0,7);
        $filename = $_FILES["gallery_img4"]["name"];
        $tmp_name = $_FILES["gallery_img4"]["tmp_name"];
        $gallery_img3 = $random.$filename;
        $galleryimage4 = '../uploadedimages/'.$gallery_img4;
        move_uploaded_file($tmp_name,$galleryimage4);
        $oldgallery_img4 = $_POST["gallery_img4"]; 
        $oldgalleryimage_url4 = '../uploadedimages/'.$oldgallery_img4;
         //// getting already existing file for unlinking
        if(file_exists($oldgalleryimage_url4))
        {
          unlink($oldgalleryimage_url4);
        }
        unset($oldgallery_img4); // undefining that variable
      }
   }
   else
   {
      $random = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyz"),0,7);
      $filename = $_FILES["gallery_img4"]["name"];
      $tmp_name = $_FILES["gallery_img4"]["tmp_name"];
      $gallery_img4 = $random.$filename;
      
      $galleryimage4 = '../uploadedimages/'.$gallery_img4;
      move_uploaded_file($tmp_name,$galleryimage4);
      $oldgallery_img4 = $_POST["gallery_img4"];     //// getting already existing file for unlinking
      $oldgalleryimage_url4 = '../uploadedimages/'. $oldgallery_img4; 
              //// getting already existing file for unlinking
      if(file_exists($oldgalleryimage_url4))
      {
        unlink($oldgalleryimage_url4);
      }
      unset($oldgallery_img4); // undefining that variable
   }
}
//gallery_img4 ends

//gallery_img5 starts
if(empty($_FILES["gallery_img5"]["name"]))
{
 
  $gallery_img5 = $_POST["gallery_img5"];

}

else
{
  $gallery_img5 = $_POST["gallery_img5"];

  if($status=="1") // for checking if it is a copied image
  {
    
      if(in_array($gallery_img5,$imageresult)) // copied image and update first Time
      {
        
        $random = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyz"),0,7);
        $filename = $_FILES["gallery_img5"]["name"];
        $tmp_name = $_FILES["gallery_img5"]["tmp_name"];
        $gallery_img5 = $random.$filename;
        $galleryimage5 = '../uploadedimages/'.$gallery_img5;
        move_uploaded_file($tmp_name,$galleryimage5);
      }
      else // copied files and updated second time
      {
       
        $random = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyz"),0,7);
        $filename = $_FILES["gallery_img5"]["name"];
        $tmp_name = $_FILES["gallery_img5"]["tmp_name"];
        $gallery_img5 = $random.$filename;
        
        $galleryimage5 = '../uploadedimages/'.$gallery_img5;
        move_uploaded_file($tmp_name,$galleryimage5);
        $oldgallery_img5 = $_POST["gallery_img5"]; 
        $oldgalleryimage_url5 = '../uploadedimages/'.$oldgallery_img5;
         //// getting already existing file for unlinking
        if(file_exists($oldgalleryimage_url5))
        {
          unlink($oldgalleryimage_url5);
        }
        unset($oldgallery_img5); // undefining that variable
      }
   }
   else
   {
     
      $random = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyz"),0,7);
      $filename = $_FILES["gallery_img5"]["name"];
      $tmp_name = $_FILES["gallery_img5"]["tmp_name"];
      $gallery_img5 = $random.$filename;
      
      $galleryimage5 = '../uploadedimages/'.$gallery_img5;
      move_uploaded_file($tmp_name,$galleryimage5);
      $oldgallery_img5 = $_POST["gallery_img5"];     //// getting already existing file for unlinking
      $oldgalleryimage_url5 = '../uploadedimages/'. $oldgallery_img5; 
              //// getting already existing file for unlinking
      if(file_exists($oldgalleryimage_url5))
      {
        unlink($oldgalleryimage_url5);
      }
      unset($oldgallery_img5); // undefining that variable
   }
}



//gallery_img5 ends

//gallery_img6 starts
//gallery_img5 starts
if(empty($_FILES["gallery_img6"]["name"]))
{
 
  $gallery_img6 = $_POST["gallery_img6"];

}

else
{
  $gallery_img6 = $_POST["gallery_img6"];

  if($status=="1") // for checking if it is a copied image
  {
    
      if(in_array($gallery_img6,$imageresult)) // copied image and update first Time
      {
        
        $random = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyz"),0,7);
        $filename = $_FILES["gallery_img6"]["name"];
        $tmp_name = $_FILES["gallery_img6"]["tmp_name"];
        $gallery_img6 = $random.$filename;
        $galleryimage6 = '../uploadedimages/'.$gallery_img6;
        move_uploaded_file($tmp_name,$galleryimage6);
      }
      else // copied files and updated second time
      {
       
        $random = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyz"),0,7);
        $filename = $_FILES["gallery_img6"]["name"];
        $tmp_name = $_FILES["gallery_img6"]["tmp_name"];
        $gallery_img6 = $random.$filename;
        
        $galleryimage6 = '../uploadedimages/'.$gallery_img6;
        move_uploaded_file($tmp_name,$galleryimage6);
        $oldgallery_img6 = $_POST["gallery_img6"]; 
        $oldgalleryimage_url6 = '../uploadedimages/'.$oldgallery_img6;
         //// getting already existing file for unlinking
        if(file_exists($oldgalleryimage_url6))
        {
          unlink($oldgalleryimage_url6);
        }
        unset($oldgallery_img6); // undefining that variable
      }
   }
   else
   {
     
      $random = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyz"),0,7);
      $filename = $_FILES["gallery_img6"]["name"];
      $tmp_name = $_FILES["gallery_img6"]["tmp_name"];
      $gallery_img6 = $random.$filename;
      
      $galleryimage6 = '../uploadedimages/'.$gallery_img6;
      move_uploaded_file($tmp_name,$galleryimage6);
      $oldgallery_img6 = $_POST["gallery_img6"];     //// getting already existing file for unlinking
      $oldgalleryimage_url6 = '../uploadedimages/'. $oldgallery_img6; 
              //// getting already existing file for unlinking
      if(file_exists($oldgalleryimage_url6))
      {
        unlink($oldgalleryimage_url6);
      }
      unset($oldgallery_img6); // undefining that variable
   }
}

//gallery_img6 ends




  if($theme=="1")
  {

    $updateresult = mysqli_query($con,"update spa_templates set sitename='$sitename',
      logo = '$logoname',
      logo_text='$logo_text',
      url='$url',
      title='$title',
      heading='$heading',
      caption='$caption',
      about='$about',

      services='$services',
      service_icon1= '$service_icon1',
      service_text1 = '$service_text1',
      service_description1='$service_description1',
      service_icon2='$service_icon2',
      service_text2='$service_text2',
      service_description2='$service_description2',

      features='$features',
      feature_icon1='$feature_icon1',
      feature_text1='$feature_text1',
      feature_description1='$feature_description1',
      feature_icon2='$feature_icon2',
      feature_text2='$feature_text2',
      feature_description2='$feature_description2',
      feature_icon3='$feature_icon3',
      feature_text3='$feature_text3',
      feature_description3='$feature_description3',
      feature_icon4='$feature_icon4',
      feature_text4='$feature_text4',
      feature_description4='$feature_description4',
      feature_icon5='$feature_icon5',
      feature_text5='$feature_text5',
      feature_description5='$feature_description5',
      feature_icon6 ='$feature_icon6',
      feature_text6='$feature_text6',
      feature_description6='$feature_description6',

      testimonial1='$testimonial1',
      testimonial_name1='$testimonial_name1',
      testimonial_icon1='$testimonial_icon1',
      testimonial2='$testimonial2',
      testimonial_name2='$testimonial_name2',
      testimonial_icon2='$testimonial_icon2',
      testimonial3='$testimonial3',
      testimonial_name3='$testimonial_name3',
      testimonial_icon3='$testimonial_icon3',
      testimonial_icon4='$testimonial_icon4',
      testimonial4='$testimonial4',
      testimonial_name4='$testimonial_name4',

      happy_clients='$happy_clients',
      peoples_love='$peoples_love',

      gallery_description='$gallery_description',
      gallery_img1='$gallery_img1',
      gallery_img2='$gallery_img2',
      gallery_img3='$gallery_img3',
      gallery_img4='$gallery_img4',
      gallery_img5='$gallery_img5',
      gallery_img6='$gallery_img6',

      address='$address',
      place='$place',
      zip='$zip',
      hours='$hours',
      phone='$phone',
      email='$email',
      theme_name='$theme'
       where id='$id'");
  }


  else if($theme=="2")
  {

    $updateresult = mysqli_query($con,"update spa_templates set sitename='$sitename',
      logo = '$logoname',
      logo_text='$logo_text',
      url='$url',
      title='$title',
      heading='$heading',
      caption='$caption',
      about='$about',

      services='$services',
      service_icon1= '$service_icon1',
      service_text1 = '$service_text1',
      service_description1='$service_description1',
      service_icon2='$service_icon2',
      service_text2='$service_text2',
      service_description2='$service_description2',
      service_icon3='$service_icon3',
      service_text3='$service_text3',
      service_description3='$service_description3',

      features='$features',
      feature_icon1='$feature_icon1',
      feature_text1='$feature_text1',
      feature_description1='$feature_description1',
      feature_icon2='$feature_icon2',
      feature_text2='$feature_text2',
      feature_description2='$feature_description2',
      feature_icon3='$feature_icon3',
      feature_text3='$feature_text3',
      feature_description3='$feature_description3',
      feature_icon4='$feature_icon4',
      feature_text4='$feature_text4',
      feature_description4='$feature_description4',
      feature_icon5='$feature_icon5',
      feature_text5='$feature_text5',
      feature_description5='$feature_description5',
      feature_icon6 ='$feature_icon6',
      feature_text6='$feature_text6',
      feature_description6='$feature_description6',
      
      team_image1='$team_image1',
      team_name1='$team_name1',
      team_role1='$team_role1',
      team_image2='$team_image2',
      team_name2='$team_name2',
      team_role2='$team_role2',
      team_image3='$team_image3',
      team_name3='$team_name3',
      team_role3='$team_role3',

      happy_clients='$happy_clients',
      peoples_love='$peoples_love',

      gallery_description='$gallery_description',
      gallery_img1='$gallery_img1',
      gallery_img2='$gallery_img2',
      gallery_img3='$gallery_img3',
      gallery_img4='$gallery_img4',
      gallery_img5='$gallery_img5',
      gallery_img6='$gallery_img6',
      gallery_img7='$gallery_img7',
      gallery_img8='$gallery_img8',

      address='$address',
      place='$place',
      zip='$zip',
      hours='$hours',
      phone='$phone',
      email='$email',
      theme_name='$theme'
       where id='$id'");


  } 
  else
  {

  } 


    if($updateresult=="1")
    {
      echo "updated successfully";

    }
    else
    {
      echo "Failed to update";   
    }
?>
    