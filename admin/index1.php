<?php
session_start();
if($_SESSION['type']=='Admin')
{
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Required meta tags-->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Colorlib Templates">
    <meta name="author" content="Colorlib">
    <meta name="keywords" content="Colorlib Templates">

    <!-- Title Page-->
    <title>Form</title>

    <!-- Icons font CSS-->
    <link href="../vendor1/mdi-font/css/material-design-iconic-font.min.css" rel="stylesheet" media="all">
    <link href="../vendor1/font-awesome-4.7/css/font-awesome.min.css" rel="stylesheet" media="all">
    <!-- Font special for pages-->
    <link href="https://fonts.googleapis.com/css?family=Poppins:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

    <!-- Vendor CSS-->
    <link href="../vendor1/select2/select2.min.css" rel="stylesheet" media="all">
    <link href="../vendor1/datepicker/daterangepicker.css" rel="stylesheet" media="all">

    <!-- Main CSS-->
    <link href="../css1/main.css" rel="stylesheet" media="all">
</head>

<body>
    <?php
    $lastid = $_GET["lastid"];
    ?>
    <div class="page-wrapper bg-gra-02 p-t-130 p-b-100 font-poppins">
        <div class="wrapper wrapper--w680">
            <div class="card card-4">
                <div class="card-body">
                	<h1 class="title">Basic Information</h1>
                    <form method="POST" action = "connect2.php" enctype="multipart/form-data">
                    	
                               <h1 class="title border">Testimonials</h1>
                
                             <div class="row row-space">
                            <div class="col-3">
                                <div class="input-group">
                                    <label class="label">Testimonial Icon 1</label>
                                    <input class="input--style-4" type="file" name="testimonial_icon1">
                                </div>
                            </div>
                            
                            <div class="col-3">
                                <div class="input-group">
                                    <label class="label">Testimonial1</label>
                                    <input class="input--style-4" type="text" name="testimonial1">
                                </div>
                            </div>
                             
                        <div class="col-3">
                                <div class="input-group">
                                    <label class="label">Testimonial Name 1</label>
                                    <input class="input--style-4" type="text" name="testimonial_name1">
                                </div>
                            </div>
                               </div>
                           
                            <div class="row row-space">
                            <div class="col-3">
                                <div class="input-group">
                                    <label class="label">Testimonial Icon 2</label>
                                    <input class="input--style-4" type="file" name="testimonial_icon2">
                                </div>
                            </div>
                            
                            <div class="col-3">
                                <div class="input-group">
                                    <label class="label">Testimonial2</label>
                                    <input class="input--style-4" type="text" name="testimonial2">
                                </div>
                            </div>
                             
                        <div class="col-3">
                                <div class="input-group">
                                    <label class="label">Testimonial Name 2</label>
                                    <input class="input--style-4" type="text" name="testimonial_name2">
                                </div>
                            </div>
                               </div>
                                <div class="row row-space">
                            <div class="col-3">
                                <div class="input-group">
                                    <label class="label">Testimonial Icon 3</label>
                                    <input class="input--style-4" type="file" name="testimonial_icon3">
                                </div>
                            </div>
                            
                            <div class="col-3">
                                <div class="input-group">
                                    <label class="label">Testimonial3</label>
                                    <input class="input--style-4" type="text" name="testimonial3">
                                </div>
                            </div>
                             
                        <div class="col-3">
                                <div class="input-group">
                                    <label class="label">Testimonial Name 3</label>
                                    <input class="input--style-4" type="text" name="testimonial_name3">
                                </div>
                            </div>
                               </div>

                                <div class="row row-space">
                            <div class="col-3">
                                <div class="input-group">
                                    <label class="label">Testimonial Icon 4</label>
                                    <input class="input--style-4" type="file" name="testimonial_icon4">
                                </div>
                            </div>
                            
                            <div class="col-3">
                                <div class="input-group">
                                    <label class="label">Testimonial4</label>
                                    <input class="input--style-4" type="text" name="testimonial4">
                                </div>
                            </div>
                             
                        <div class="col-3">
                                <div class="input-group">
                                    <label class="label">Testimonial Name 4</label>
                                    <input class="input--style-4" type="text" name="testimonial_name4">
                                </div>
                            </div>
                               </div>


                                <h1 class="title border">Counter Section</h1>
                    
                        <div class="row row-space">
                            <div class="col-2">
                                <div class="input-group">
                                    <label class="label">Happy Clients</label>
                                    <input class="input--style-4" type="number" name="happy_clients">
                                </div>
                            </div>
                            <div class="col-2">
                                <div class="input-group">
                                    <label class="label">People's Love</label>
                                    <input class="input--style-4" type="text" name="peoples_love">
                                </div>
                            </div>
                           
                        </div>


                                <h1 class="title border">Counter Section</h1>
                    
                        <div class="row row-space">
                            <div class="col-1">
                                <div class="input-group">
                                    <label class="label">Gallery Description</label>
                                    <textarea rows="5" cols="30"  name="gallery_description"></textarea>
                                </div>
                            </div>
                            

                           
                        </div>
                           <div class="row row-space">
                            <div class="col-3">
                                <div class="input-group">
                                    <label class="label">Gallery Image 1</label>
                                    <input class="input--style-4" type="file" name="gallery_img1">
                                </div>
                            </div>
                            <div class="col-3">
                                <div class="input-group">
                                    <label class="label">Gallery Image 2</label>
                                    <input class="input--style-4" type="file" name="gallery_img2">
                                </div>
                            </div>
                            <div class="col-3">
                                <div class="input-group">
                                    <label class="label">Gallery Image 3</label>
                                    <input class="input--style-4" type="file" name="gallery_img3">
                                </div>
                            </div>

                        </div>
                          <div class="row row-space">
                            <div class="col-3">
                                <div class="input-group">
                                    <label class="label">Gallery Image 4</label>
                                    <input class="input--style-4" type="file" name="gallery_img4">
                                </div>
                            </div>
                            <div class="col-3">
                                <div class="input-group">
                                    <label class="label">Gallery Image 5</label>
                                    <input class="input--style-4" type="file" name="gallery_img5">
                                </div>
                            </div>
                            <div class="col-3">
                                <div class="input-group">
                                    <label class="label">Gallery Image 6</label>
                                    <input class="input--style-4" type="file" name="gallery_img6">
                                </div>
                            </div>

                        </div>


                       

                        <div class="p-t-15">
                            <input class="btn btn--radius-2 btn--blue" type="submit" id="submit" name="submit">
                            <input type="hidden" name="lastid" value="<?php echo $lastid;?>">
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <!-- Jquery JS-->
    <script src="../vendor1/jquery/jquery.min.js"></script>
    <!-- Vendor JS-->
    <script src="../vendor1/select2/select2.min.js"></script>
    <script src="../vendor1/datepicker/moment.min.js"></script>
    <script src="../vendor1/datepicker/daterangepicker.js"></script>

    <!-- Main JS-->
    <script src="../js1/global.js"></script>
    <script src="../js1/validate.js"></script>
</body><!-- This templates was made by Colorlib (https://colorlib.com) -->

</html>
<?php
}
else
{
 header("location:index.php");
}
?>
<!-- end document-->